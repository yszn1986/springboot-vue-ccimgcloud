# Springboot-vue-ccimgcloud

#### 介绍
一个仿照小红书设计的浏览图片uniapp项目,数据库和apk测试包在ccimg-admin-vue下的public包中，有问题加q3044606584。

 第一次学习uniapp因此想做个项目来学习一下，后续可能更新会很慢，如果觉得项目不错可以点个赞，谢谢。

####文档
等把所有的功能完善后会更新一份文档...。如果有小伙伴想一起完善这个项目也可以加q，目前后端管理员的功能未实现，后续再慢慢做吧，不太想做后端页面。

#### 功能介绍
 - 使用springboot+mybatis_plus+vue+uniapp框架
 - 图片上传发布
 - 图片下载收藏
 - 使用推荐算法做首页推荐功能
 - 评论跳转回复
 - 用户关注点赞
 - 使用es做搜索功能
 - 私信聊天，消息发送
 - oss对象存储
 - 邮箱短信验证发送

 !!! 后续将实现更多的功能，也会慢慢的完善bug，因为做得时间很短，所以很多
 代码写的不规范也没有时间去整理。如果有bug可以加q联系我。


#### 实现页面
#### 首页
![输入图片说明](./ccimg-admin-vue/public/img/1.png)
#### 关注页面
![输入图片说明](./ccimg-admin-vue/public/img/4.png)
#### 热榜
![输入图片说明](./ccimg-admin-vue/public/img/20.jpg)
#### 分类
![输入图片说明](./ccimg-admin-vue/public/img/2.png)
#### 详情
![输入图片说明](./ccimg-admin-vue/public/img/3.png)
#### 发布
![输入图片说明](./ccimg-admin-vue/public/img/5.png)
#### 消息
![输入图片说明](./ccimg-admin-vue/public/img/10.jpg)
#### 发送消息
![输入图片说明](./ccimg-admin-vue/public/img/18.jpg)
#### 赞和收藏
![输入图片说明](./ccimg-admin-vue/public/img/7.png)
#### 评论和@
![输入图片说明](./ccimg-admin-vue/public/img/11.jpg)
#### 新增关注
![输入图片说明](./ccimg-admin-vue/public/img/13.jpg)
#### 个人主页
![输入图片说明](./ccimg-admin-vue/public/img/8.png)
#### 专辑页面
![输入图片说明](./ccimg-admin-vue/public/img/15.jpg)
#### 专辑详情页面
![输入图片说明](./ccimg-admin-vue/public/img/14.jpg)
#### 编辑专辑
![输入图片说明](./ccimg-admin-vue/public/img/16.jpg)
#### 删除专辑中的图片
![输入图片说明](./ccimg-admin-vue/public/img/19.jpg)
#### 删除浏览记录
![输入图片说明](./ccimg-admin-vue/public/img/17.jpg)
#### 后台页面
![输入图片说明](./ccimg-admin-vue/public/img/9.png)

