package com.xz.platform.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class FollowVo implements Serializable {

    /**
     *
     */
    private Long uid;


    private String username;

    /**
     *
     */
    private String avatar;

    private boolean isfollow;

    private Long userId;

    private Long fanCount;

    private String time;


}
