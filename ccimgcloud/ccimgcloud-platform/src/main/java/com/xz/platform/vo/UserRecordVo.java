package com.xz.platform.vo;

import lombok.Data;

import java.util.Date;

@Data
public class UserRecordVo {

    /**
     *
     */
    private Long id;
    /**
     *
     */
    private Long uid;
    /**
     *
     */
    private Long trendCount;
    /**
     *
     */
    private Long followCount;
    /**
     *
     */
    private Long fanCount;
    /**
     *
     */
    private Long noreplyCount;
    /**
     *
     */
    private Long agreeCollectionCount;
    /**
     *
     */
    private Long creator;
    /**
     *
     */
    private Long nochatCount;
    /**
     *
     */
    private Date createDate;
}
