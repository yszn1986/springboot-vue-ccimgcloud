package com.xz.platform.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class MessageVo implements Serializable {

    private Long sendId;

    private Long acceptId;

    private String username;

    private String avatar;

    private Integer count;

    private String content;

    private String date;

    private String  time;
}
