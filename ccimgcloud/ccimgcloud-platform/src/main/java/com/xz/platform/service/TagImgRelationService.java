package com.xz.platform.service;

import com.xz.common.service.CrudService;
import com.xz.platform.dto.TagImgRelationDTO;
import com.xz.platform.entity.TagImgRelationEntity;

/**
 * 
 *
 * @author xiaozhao sunlightcs@gmail.com
 * @since 1.0.0 2023-03-16
 */
public interface TagImgRelationService extends CrudService<TagImgRelationEntity, TagImgRelationDTO> {

}