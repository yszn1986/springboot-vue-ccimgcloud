package com.xz.platform.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class TagVo implements Serializable {


    private Long count;

    private String name;


}
