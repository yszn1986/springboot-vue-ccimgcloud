package com.xz.platform.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xz.common.service.impl.CrudServiceImpl;
import com.xz.common.utils.ConvertUtils;
import com.xz.common.utils.DateUtils;
import com.xz.common.utils.PageUtils;
import com.xz.platform.dao.*;
import com.xz.platform.dto.UserDTO;
import com.xz.platform.entity.*;
import com.xz.platform.service.UserService;
import com.xz.platform.vo.TrendVo;
import com.xz.platform.vo.UserVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author xiaozhao sunlightcs@gmail.com
 * @since 1.0.0 2023-03-13
 */
@Service
public class UserServiceImpl extends CrudServiceImpl<UserDao, UserEntity, UserDTO> implements UserService {

    @Autowired
    ImgDetailsDao imgDetailsDao;

    @Autowired
    AlbumDao albumDao;

    @Autowired
    AlbumImgRelationDao albumImgRelationDao;

    @Autowired
    UserRecordDao userRecordDao;

    @Autowired
    UserDao userDao;

    @Override
    public QueryWrapper<UserEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<UserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    @Override
    public Page<TrendVo> getTrendByUser(long page, long limit, String userId) {

        List<TrendVo> res = new ArrayList<>();

        List<ImgDetailsEntity> imgDetailList = imgDetailsDao.selectList(new QueryWrapper<ImgDetailsEntity>().eq("user_id", userId).orderByDesc("update_date"));
        TrendVo trendVo = null;
        List<String> imgList = null;
        List<AlbumEntity> albumList = null;
        List<AlbumImgRelationEntity> albumImgRelationList = null;
        //分页
        for (ImgDetailsEntity model : imgDetailList) {

            trendVo = ConvertUtils.sourceToTarget(model, TrendVo.class);

            imgList = JSON.parseArray(model.getImgsUrl(), String.class);
            trendVo.setImgsUrl(imgList);
            trendVo.setMid(model.getId());


            //得到专辑
            albumList = albumDao.selectList(new QueryWrapper<AlbumEntity>().eq("uid", userId));

            for (AlbumEntity element : albumList) {
                albumImgRelationList = albumImgRelationDao.selectList(new QueryWrapper<AlbumImgRelationEntity>().eq("aid", element.getId()));
                for (AlbumImgRelationEntity e : albumImgRelationList) {
                    if (StringUtils.equals(String.valueOf(e.getMid()), String.valueOf(model.getId()))) {
                        trendVo.setAlbumId(element.getId());
                        trendVo.setAlbumName(element.getName());
                        break;
                    }
                }
            }

            trendVo.setTime(DateUtils.timeUtile(model.getUpdateDate()));
            res.add(trendVo);
        }

        return PageUtils.getPages((int) page, (int) limit, res);
    }

    @Override
    public UserVo getUserInfo(String uid) {
        UserEntity userEntity = baseDao.selectById(uid);
        UserRecordEntity userRecordEntity = userRecordDao.selectOne(new QueryWrapper<UserRecordEntity>().eq("uid", uid));
        UserVo userVo = ConvertUtils.sourceToTarget(userEntity, UserVo.class);
        userVo.setTrendCount(userRecordEntity.getTrendCount());
        userVo.setFollowCount(userRecordEntity.getFollowCount());
        userVo.setFanCount(userRecordEntity.getFanCount());
        userVo.setNoreplyCount(userRecordEntity.getNoreplyCount());
        userVo.setAgreeCollectionCount(userRecordEntity.getAgreeCollectionCount());
        return userVo;
    }

    @Override
    public List<UserVo> searchUser(String keyword) {

        List<UserEntity> userEntities = baseDao.selectList(new QueryWrapper<UserEntity>().like("user_id", keyword).or().like("username", keyword));

        List<UserVo> list = new ArrayList<>();
        UserVo userVo = null;
        UserRecordEntity userRecordEntity = null;
        for (UserEntity model : userEntities) {
            userVo = ConvertUtils.sourceToTarget(model, UserVo.class);
            userRecordEntity = userRecordDao.selectOne(new QueryWrapper<UserRecordEntity>().eq("uid", model.getId()));
            userVo.setFanCount(userRecordEntity.getFanCount());
            list.add(userVo);
        }
        return list;
    }

    @Override
    public UserEntity updateUser(UserEntity userEntity) {
        userDao.updateById(userEntity);
        return userEntity;
    }

}