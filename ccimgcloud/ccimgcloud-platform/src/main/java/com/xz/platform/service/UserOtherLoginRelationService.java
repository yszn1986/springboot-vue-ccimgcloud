package com.xz.platform.service;

import com.xz.common.service.CrudService;
import com.xz.platform.dto.UserOtherLoginRelationDTO;
import com.xz.platform.entity.UserOtherLoginRelationEntity;

/**
 * 
 *
 * @author xiaozhao sunlightcs@gmail.com
 * @since 1.0.0 2023-03-16
 */
public interface UserOtherLoginRelationService extends CrudService<UserOtherLoginRelationEntity, UserOtherLoginRelationDTO> {

}