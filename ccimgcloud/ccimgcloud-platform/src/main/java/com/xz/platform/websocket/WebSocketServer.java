package com.xz.platform.websocket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author:gzh
 * @Date: 2022/4/20 20:27
 * 注意在websocket通信中只能传string
 */
@ServerEndpoint("/ws/{userId}")
@Component
@Slf4j
public class WebSocketServer {

    /**
     *
     */
    public static final ConcurrentHashMap<String, Session> sessionMap = new ConcurrentHashMap<>();


    /***
     * 1.把登录用户存到sessionMap中
     * 2.发送给所有人当前登录人员信息
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("userId") String userId) {

        log.info("现在来连接的客户id：" + session.getId() + "用户名：" + userId);

        if (sessionMap.containsKey(userId)) {
            onClose(sessionMap.get(userId));
        }
        sessionMap.put(userId, session);
    }

    //关闭连接
    /**
     * 1.把登出的用户从sessionMap中剃除
     * 2.发送给所有人当前登录人员信息
     */
    @OnClose
    public void onClose(@PathParam("userId") String userId, Session session) throws Exception {
        log.info(session.getRequestURI().getPath() + "，关闭连接开始：" + session.getId());
        sessionMap.remove(userId);
        log.info(session.getRequestURI().getPath() + "，关闭连接完成：" + session.getId());
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        log.info("前台发送消息：" + message);
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error(error.toString());
    }

    public void onClose(Session session) {
        //判断当前连接是否在线
//        if (!session.isOpen()) {
//            return;
//        }
        try {
            session.close();
        } catch (IOException e) {
            log.error("关闭连接异常：" + e);
        }
    }

    public static void sendMessage(String message, Session session) {
        try {
            session.getAsyncRemote().sendText(message);
            log.info("推送成功：" + message);
        } catch (Exception e) {
            log.error("推送异常：" + e);
        }
    }

    public static void sendMessageTo(String message, String toUserId) {
        try {
            Session session = sessionMap.get(toUserId);
            session.getAsyncRemote().sendText(message);
            log.info("推送成功：" + message);
        } catch (Exception e) {
            log.error("推送异常：" + e);
        }
    }

}
