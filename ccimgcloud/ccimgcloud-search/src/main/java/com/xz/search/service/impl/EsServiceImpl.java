package com.xz.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.xz.search.config.EsConstant;
import com.xz.search.service.EsService;
import com.xz.search.utils.EsClientUtil;
import com.xz.search.vo.ImgDetailSearchVo;
import org.apache.lucene.search.TotalHits;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.core.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class EsServiceImpl implements EsService {

    @Autowired
    RestHighLevelClient restHighLevelClient;

    @Override
    public HashMap<String, Object> esSearch(long page, long limit, String keyword) throws IOException {
        HashMap<String, Object> map = new HashMap<>();

        SearchRequest searchRequest = new SearchRequest(EsConstant.INDEX);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 精确查询，添加查询条件
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        MatchQueryBuilder contentQueryBuilder = QueryBuilders.matchQuery("content", keyword);
        MatchQueryBuilder usernameQueryBuilder = QueryBuilders.matchQuery("username", keyword);
        BoolQueryBuilder should = boolQueryBuilder.should(contentQueryBuilder).should(usernameQueryBuilder);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        searchSourceBuilder.query(should);    // 分页

        searchSourceBuilder.from((int) (page - 1) * (int) limit);
        searchSourceBuilder.size((int) limit);    // 高亮 =========
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);    // 解析结果 ==========
        SearchHits hits = searchResponse.getHits();
        TotalHits totalHits = hits.getTotalHits();

        System.out.println(totalHits.value);

        List<ImgDetailSearchVo> res = new ArrayList<>();

        for (SearchHit documentFields : hits.getHits()) {
            // 使用新的字段值（高亮），覆盖旧的字段值
            String sourceAsString = documentFields.getSourceAsString();
            ImgDetailSearchVo imgDetailSearchVo = JSON.parseObject(sourceAsString, ImgDetailSearchVo.class);
            res.add(imgDetailSearchVo);
        }
        map.put("records", res);
        map.put("total", totalHits.value);
        return map;
    }

    @Override
    public void addData(ImgDetailSearchVo imgDetailSearchVo) throws IOException {
// 创建请求
        IndexRequest request = new IndexRequest(EsConstant.INDEX);

        // 制定规则 PUT /liuyou_index/_doc/1
        request.id(String.valueOf(imgDetailSearchVo.getId()));// 设置文档ID
        request.timeout(TimeValue.timeValueMillis(1000));// request.timeout("1s")
        // 将我们的数据放入请求中
        request.source(JSON.toJSONString(imgDetailSearchVo), XContentType.JSON);
        // 客户端发送请求，获取响应的结果
        IndexResponse response = restHighLevelClient.index(request, RequestOptions.DEFAULT);
        System.out.println(response.status());// 获取建立索引的状态信息 CREATED
        System.out.println(response);// 查看返回内容
    }

    @Override
    public void addbulkData(List<ImgDetailSearchVo> dataList) throws IOException {

        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.timeout("2m"); // 可更具实际业务是指
        for (int i = 0; i < dataList.size(); i++) {
            bulkRequest.add(
                    new IndexRequest(EsConstant.INDEX)
                            .id(String.valueOf(dataList.get(i).getId()))
                            .source(JSON.toJSONString(dataList.get(i)), XContentType.JSON)
            );
        }
        restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
//        restHighLevelClient.close();
    }

    /**
     * 删除文档中的数据
     *
     * @param id 文档
     * @throws IOException 异常
     */
    @Override
    public void delData(String id) throws IOException {
        DeleteRequest request = new DeleteRequest(EsConstant.INDEX, id);
        request.timeout("1s");
        restHighLevelClient.delete(request, RequestOptions.DEFAULT);
    }

    @Override
    public void update(ImgDetailSearchVo imgDetailSearchVo) throws IOException {
        String voStr = JSON.toJSONString(imgDetailSearchVo);
        EsClientUtil.updateDocByJson(EsConstant.INDEX, String.valueOf(imgDetailSearchVo.getId()), voStr);
    }
}
