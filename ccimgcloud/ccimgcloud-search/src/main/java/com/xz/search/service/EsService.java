package com.xz.search.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xz.search.entity.ImgDetailSearchEntity;
import com.xz.search.vo.ImgDetailSearchVo;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public interface EsService {
    HashMap<String,Object> esSearch(long page, long limit, String keyword) throws IOException;

    void addData(ImgDetailSearchVo imgDetailSearchVo) throws IOException;

    void addbulkData(List<ImgDetailSearchVo> dataList) throws IOException;

    void delData(String id) throws IOException;

    void update(ImgDetailSearchVo imgDetailSearchVo) throws IOException;
}
