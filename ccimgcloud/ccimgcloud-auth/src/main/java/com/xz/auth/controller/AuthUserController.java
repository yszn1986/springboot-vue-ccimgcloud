package com.xz.auth.controller;

import com.xkcoding.http.config.HttpConfig;
import com.xz.auth.dto.AuthUserDTO;
import com.xz.auth.entity.AuthUser;
import com.xz.auth.service.AuthUserService;
import com.xz.common.utils.Result;
import com.xz.common.validator.ValidatorUtils;
import com.xz.common.validator.group.DefaultGroup;
import me.zhyd.oauth.config.AuthConfig;
import me.zhyd.oauth.enums.scope.AuthGiteeScope;
import me.zhyd.oauth.enums.scope.AuthGithubScope;
import me.zhyd.oauth.enums.scope.AuthWeiboScope;
import me.zhyd.oauth.exception.AuthException;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.request.*;
import me.zhyd.oauth.utils.AuthScopeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Arrays;
import java.util.Map;

/**
 * @author 48423
 */
@RestController
@RequestMapping("auth")
public class AuthUserController {

    @Autowired
    AuthUserService authUserService;

    /**
     * 登录功能
     * @param authUserDTO 用户类
     */
    @RequestMapping("login")
    public Result<?> login(@RequestBody AuthUserDTO authUserDTO) {

        Map<String, Object> map = authUserService.login(authUserDTO);

        return new Result<Map<String, Object>>().ok(map);
    }


    /**
     * 根据用户的token信息得到当前用户
     * @param token token信息
     * @return 用户类
     */
    @RequestMapping("getUserInfoByToken")
    public Result<?> getUserInfoByToken(String token) {
        AuthUser authUser = authUserService.getUserInfoByToken(token);
        return new Result<AuthUser>().ok(authUser);
    }



    /**
     * 用户注册
     * @param authUserDTO 前台传递用户信息
     */
    @RequestMapping("register")
    public Result<?> register(@RequestBody AuthUserDTO authUserDTO) {
        ValidatorUtils.validateEntity(authUserDTO, DefaultGroup.class);
        Map<String, Object> data = authUserService.regist(authUserDTO);
        return new Result<Map<String, Object>>().ok(data);

    }


    @RequestMapping("loginOut")
    public Result<?> loginOut(@RequestBody AuthUser authUser ){
        authUserService.loginOut(authUser);
        return new Result<>().ok();
    }


    /**
     *第三方登录功能
     */
    @RequestMapping("/render/{source}")
    public Result renderAuth(@PathVariable("source") String source, HttpServletResponse response) throws IOException {
//        log.info("进入render：" + source);
//        AuthRequest authRequest = getAuthRequest(source);
//        String authorizeUrl = authRequest.authorize(AuthStateUtils.createState());
//        log.info(authorizeUrl);
//        return new Result<>().ok(authorizeUrl);
        return null;
    }

    /**
     * 第三方登录功能
     */
    @RequestMapping("/callback/{source}")
    public void loginBySource(@PathVariable("source") String source, AuthCallback callback, HttpServletRequest request, HttpServletResponse rep) throws IOException {
//        log.info("进入callback：" + source + " callback params：" + JSONObject.toJSONString(callback));
//        AuthRequest authRequest = getAuthRequest(source);
//        AuthResponse<AuthUser> response = authRequest.login(callback);
//        log.info(JSONObject.toJSONString(response));
//        if (response.ok()) {
//            //得到第三方登录用户的基本信息
//            AuthUser user = response.getData();
//            /*先在数据库中查找是否存在当前用户
//             *
//             * 如果当前用户不存在
//             *  先往第三方登录表中插入信息
//             *  再往用户表中插入信息
//             */
//            UserEntity userEntity = null;
//            UserThirdPartRelationEntity entity = userThirdPartRelationService.getOne(new QueryWrapper<UserThirdPartRelationEntity>().eq("third_part_user_id", user.getUuid()));
//            if (entity != null) {
//                userEntity = userService.getById(entity.getUserId());
//            } else {
//                UserThirdPartRelationEntity model = userThirdPartRelationService.getOne(new QueryWrapper<UserThirdPartRelationEntity>().eq("email", user.getEmail()).or().eq("phone", ""));
//                if (model != null) {
//                    model.setThirdPartUserId(user.getUuid());
//                    model.setThirdPartUsername(user.getUsername());
//                    model.setThirdPartAvatar(user.getAvatar());
//                    userThirdPartRelationService.updateById(model);
//                    userEntity = userService.getById(model.getUserId());
//                } else {
//                    userEntity = new UserEntity();
//                    userEntity.setUsername(user.getUsername());
//                    userEntity.setAvatar(user.getAvatar());
//                    userEntity.setEmail(user.getEmail());
//                    userService.save(userEntity);
//
//                    UserThirdPartRelationEntity userThirdPartRelationEntity = new UserThirdPartRelationEntity();
//                    userThirdPartRelationEntity.setUserId(userEntity.getId());
//                    userThirdPartRelationEntity.setThirdPartUserId(user.getUuid());
//                    userThirdPartRelationEntity.setThirdPartUsername(user.getUsername());
//                    userThirdPartRelationEntity.setThirdPartAvatar(user.getAvatar());
//                    userThirdPartRelationEntity.setEmail(user.getEmail());
//                    userThirdPartRelationService.save(userThirdPartRelationEntity);
//                }
//            }
//            //将用户信息保存再redis中
//            redisUtils.set(userEntity.getId() + ":" + userEntity.getUsername(), JSONObject.toJSONString(userEntity));
//            ThreadLocalUtils.put(userEntity.getId(),userEntity);
//            String token = JwtUtils.getJwtToken(userEntity.getUsername(), "12345");
//            rep.sendRedirect("http://node3.js.giao.me:9956/dashboard/index?token=" + token);
    // }
    }

    /**
     * 第三方登录辅助方法
     */
    private AuthRequest getAuthRequest(String source) {
        AuthRequest authRequest = null;
        switch (source.toLowerCase()) {
            case "github":
                authRequest = new AuthGithubRequest(AuthConfig.builder()
                        .clientId("")
                        .clientSecret("")
                        .redirectUri("http://localhost:8443/oauth/callback/github")
                        .scopes(AuthScopeUtils.getScopes(AuthGithubScope.values()))
                        // 针对国外平台配置代理
                        .httpConfig(HttpConfig.builder()
                                .timeout(15000)
                                .proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("127.0.0.1", 10080)))
                                .build())
                        .build());
                break;
            case "gitee":
                authRequest = new AuthGiteeRequest(AuthConfig.builder()
                        .clientId("9e3e980b7f65a4125ea75f54a398229434f5b3bb20fd2d20c1afd4737033a68f")
                        .clientSecret("d52c7e4df06ef2067fdb3a79d8e98249b2195c61f4fd5cee469388a3a6a10cf1")
                        .redirectUri("http://node3.js.giao.me:9956/gitee/video/front/user/callback/gitee")
                        .scopes(AuthScopeUtils.getScopes(AuthGiteeScope.values()))
                        .build());
                break;
            case "weibo":
                authRequest = new AuthWeiboRequest(AuthConfig.builder()
                        .clientId("")
                        .clientSecret("")
                        .redirectUri("http://dblog-web.zhyd.me/oauth/callback/weibo")
                        .scopes(Arrays.asList(
                                AuthWeiboScope.EMAIL.getScope(),
                                AuthWeiboScope.FRIENDSHIPS_GROUPS_READ.getScope(),
                                AuthWeiboScope.STATUSES_TO_ME_READ.getScope()
                        ))
                        .build());
                break;
            case "qq":
                authRequest = new AuthQqRequest(AuthConfig.builder()
                        .clientId("")
                        .clientSecret("")
                        .redirectUri("http://localhost:8443/oauth/callback/qq")
                        .build());
                break;
            case "wechat_open":
                authRequest = new AuthWeChatOpenRequest(AuthConfig.builder()
                        .clientId("")
                        .clientSecret("")
                        .redirectUri("http://www.zhyd.me/oauth/callback/wechat")
                        .build());
                break;
            case "csdn":
                authRequest = new AuthCsdnRequest(AuthConfig.builder()
                        .clientId("")
                        .clientSecret("")
                        .redirectUri("http://dblog-web.zhyd.me/oauth/callback/csdn")
                        .build());
                break;
            default:
                break;
        }
        if (null == authRequest) {
            throw new AuthException("未获取到有效的Auth配置");
        }
        return authRequest;
    }
}
