package com.xz.auth.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xz.auth.constant.AuthConstant;
import com.xz.auth.dao.UserRecordDao;
import com.xz.auth.entity.UserRecordEntity;
import com.xz.auth.redis.RedisUtils;
import com.xz.auth.dao.AuthUserDao;
import com.xz.auth.dto.AuthUserDTO;
import com.xz.auth.entity.AuthUser;
import com.xz.auth.service.AuthUserService;
import com.xz.common.constant.Constant;
import com.xz.common.service.impl.CrudServiceImpl;
import com.xz.common.utils.ConvertUtils;
import com.xz.common.utils.JwtUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 48423
 */
@Service
public class AuthUserServiceImpl extends CrudServiceImpl<AuthUserDao, AuthUser, AuthUserDTO> implements AuthUserService {

    @Autowired
    RedisUtils redisUtils;

    @Autowired
    UserRecordDao userRecordDao;

    private static final  String RES = "res";
    private static final  String MESSAGE = "message";
    private static final String USER_KEY = "user:";
    private static final String USER_INFO = "userInfo";

    @Override
    public QueryWrapper<AuthUser> getWrapper(Map<String, Object> params) {
        return null;
    }

    @Override
    public Map<String, Object> login(AuthUserDTO authUserDTO) {

        Map<String, Object> map = new HashMap<>(2);
        String  title= authUserDTO.getUsername();
        //查询当前用户
        AuthUser authUser = baseDao.selectOne(new QueryWrapper<AuthUser>().eq("username", title).or().eq("phone", title).or().eq("email", title));

        String s = SecureUtil.md5(authUserDTO.getPassword());
        if (ObjectUtil.isEmpty(authUser) || !s.equals(authUser.getPassword())) {
            map.put(RES, AuthConstant.ERROR_STATUE.toString());
            map.put(MESSAGE, AuthConstant.ReturnMessage.LOGIN_ERROR.getValue());
            return map;
        }
        String token = JwtUtils.getJwtToken(String.valueOf(authUser.getUserId()), authUser.getPassword());
        //将用户信息保存在redis中
        redisUtils.set(USER_KEY + authUser.getUserId(), JSONObject.toJSONString(authUser));

        map.put(Constant.FRONT_TOKEN_HEADER, token);
        map.put(USER_INFO, authUser);
        return map;
    }

    @Override
    public AuthUser getUserInfoByToken(String token) {

        String userId = JwtUtils.getUserId(token);

        return baseDao.selectOne(new QueryWrapper<AuthUser>().eq("user_id", userId));
    }

    @Override
    public void loginOut(AuthUser authUser) {
        String key = USER_KEY + authUser.getUserId();
        redisUtils.delete(key);
    }

    @Override
    public Map<String, Object> regist(AuthUserDTO authUserDTO) {
        Map<String, Object> map = new HashMap<>();
        AuthUser currentUser = baseDao.selectOne(new QueryWrapper<AuthUser>().eq("phone", authUserDTO.getPhone()).or().eq("email", authUserDTO.getEmail()));
        if (currentUser == null) {

            String code = redisUtils.get("code");
            if (StringUtils.isEmpty(code)) {
                map.put(RES, AuthConstant.ERROR_STATUE.toString());
                map.put(MESSAGE, AuthConstant.ReturnMessage.CODE_EXPIRE.getValue());
                return map;
            }
            if (!code.equals(authUserDTO.getCode())) {
                map.put(RES, AuthConstant.ERROR_STATUE.toString());
                map.put(MESSAGE, AuthConstant.ReturnMessage.CODE_ERROR.getValue());

                return map;
            }


            AuthUser authUser = ConvertUtils.sourceToTarget(authUserDTO, AuthUser.class);
            authUser.setUserId(Long.valueOf(RandomUtil.randomNumbers(10)));
            authUser.setUsername(RandomUtil.randomString(12));
            authUser.setAvatar("https://cube.elemecdn.com/3/7c/3ea6beec64369c2642b92c6726f1epng.png");
            authUser.setCover("https://cube.elemecdn.com/3/7c/3ea6beec64369c2642b92c6726f1epng.png");
            String password = SecureUtil.md5(authUserDTO.getPassword());
            authUser.setPassword(password);

            baseDao.insert(authUser);


            UserRecordEntity userRecordEntity = new UserRecordEntity();
            userRecordEntity.setUid(authUser.getId());
            userRecordDao.insert(userRecordEntity);


            //UserThirdPartRelationEntity userThirdPartRelationEntity = new UserThirdPartRelationEntity();
//            userThirdPartRelationEntity.setUserId(userEntity.getId());
//            userThirdPartRelationEntity.setPhone(userEntity.getMobile());
//            userThirdPartRelationEntity.setEmail(userEntity.getEmail());
//            userThirdPartRelationService.save(userThirdPartRelationEntity);
            map.put(RES, AuthConstant.SUCCESS_STATUE.toString());
            map.put(MESSAGE, AuthConstant.ReturnMessage.SUCCESS_REGIST.getValue());

            String token = JwtUtils.getJwtToken(String.valueOf(authUser.getUserId()), authUser.getPassword());
            //将用户信息保存在redis中
            redisUtils.set(USER_KEY + authUser.getUserId(), JSONObject.toJSONString(authUser));

            map.put(Constant.FRONT_TOKEN_HEADER, token);
            map.put(USER_INFO, authUser);

            return map;

        } else {
            map.put(RES, AuthConstant.ERROR_STATUE.toString());
            map.put(MESSAGE, AuthConstant.ReturnMessage.PHONE_REGIST.getValue());

            return map;
        }
    }

}
