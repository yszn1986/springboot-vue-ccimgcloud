package com.xz.auth.constant;

public interface AuthConstant {

    Integer ERROR_STATUE = 0;

    Integer SUCCESS_STATUE =1;


    enum ReturnMessage {
        /**
         * 暂停
         */
        LOGIN_ERROR("账号或密码错误"),
        CODE_EXPIRE("验证码过期"),
        CODE_ERROR("验证码错误"),
        PHONE_REGIST("手机号已被注册"),
        SUCCESS_REGIST("注册成功");


        private final String message;

        ReturnMessage(String message) {
            this.message = message;
        }

        public String getValue() {
            return message;
        }
    }
}
