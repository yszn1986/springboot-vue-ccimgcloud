package com.xz.recommend.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xz.common.service.impl.CrudServiceImpl;
import com.xz.common.utils.ConvertUtils;
import com.xz.common.utils.PageUtils;
import com.xz.recommend.common.client.RecommendClient;
import com.xz.recommend.common.redis.RedisUtils;
import com.xz.recommend.dao.BrowseRecordDao;
import com.xz.recommend.dao.ImgDetailsDao;
import com.xz.recommend.dao.UserDao;
import com.xz.recommend.dto.ImgDetailsDTO;
import com.xz.recommend.entity.ImgDetailsEntity;
import com.xz.recommend.entity.UserEntity;
import com.xz.recommend.service.CategoryService;
import com.xz.recommend.service.ImgDetailsService;
import com.xz.recommend.utils.PearsonUtils;
import com.xz.recommend.utils.RecommendUtils;
import com.xz.recommend.vo.ImgDetailVo;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 
 *
 * @author xiaozhao sunlightcs@gmail.com
 * @since 1.0.0 2023-03-13
 */
@Service
public class ImgDetailsServiceImpl extends CrudServiceImpl<ImgDetailsDao, ImgDetailsEntity, ImgDetailsDTO> implements ImgDetailsService {

    @Autowired
    RedisUtils redisUtils;

    @Autowired
    CategoryService categoryService;

    @Autowired
    UserDao userDao;

    @Autowired
    BrowseRecordDao browseRecordDao;

    @Autowired
    RecommendClient recommendClient;


    @Override
    public QueryWrapper<ImgDetailsEntity> getWrapper(Map<String, Object> params) {
        return null;
    }

    /**
     * @param uid 用户id
     * @return
     */
    public Map<String, HashMap<String, Integer>> createMap(String uid) {
        HashMap<String, HashMap<String, Integer>> res = new HashMap<>();
        HashMap<String, Integer> umap = new HashMap<>();
        String key = "br:" + uid + ":";
        Set<String> listKey = redisUtils.getListKey(key);
        //首先计算当前用户浏览记录，统计用户浏览记录
        if (!listKey.isEmpty()) {
            for (String k : listKey) {
                String[] split = k.split(":");
                umap.put(split[2], Integer.valueOf(redisUtils.get(k)));
            }
        }
        res.put(uid, umap);
        return res;
    }


    /**
     * 根据最近邻接点相似计算
     *
     * @param page
     * @param limit
     * @param uid
     * @return
     */
    @Override
    public Page<ImgDetailVo> recommendToUser(long page, long limit, String uid) {


        //首先计算当前用户浏览记录，统计用户浏览记录
        Map<String, HashMap<String, Integer>> map = createMap(uid);
        //如果当前用户的浏览记录为空直接返回
        if (map.get(uid).isEmpty()) {
            return null;
        }
        //统计所有二级分类
        List<String> allCategoryTwo = categoryService.getAllCategoryTwo();

        //当前用户的分数
        double[] currentList = RecommendUtils.getArray(allCategoryTwo, map.get(uid));

        //随机选择30个用户
        List<String> uids = userDao.selectRand(30);

        Map<String, Double> sourceMap = new HashMap<>();

        Map<String, HashMap<String, Integer>>  userMap = null;
        //找到相似的用户并将相似用户浏览记录中用户没有浏览的数据给用户查看
        for (String id : uids) {
            userMap = createMap(id);
            double[] otherList = RecommendUtils.getArray(allCategoryTwo, userMap.get(id));
            //计算分数
            Double relate = PearsonUtils.getPearsonCorrelationScore(currentList, otherList);
            if (!relate.isNaN()) {
                sourceMap.put(id, relate);
            }

        }

        Map<String, Double> sourceMap2 = sourceMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldVal, newVal) -> oldVal,
                        LinkedHashMap::new));

        List<ImgDetailsEntity> res = new ArrayList<>();

        //得到当前用户前20条浏览记录
        List<String> currentMidList = browseRecordDao.selectNums(uid, 20);
        List<ImgDetailsEntity> imgDetailList = baseDao.selectBatchIds(currentMidList);

        List<String> stringList = null ;
        List<ImgDetailsEntity> otherImgDetailList =null;
        for (String key : sourceMap2.keySet()) {
            if (StringUtils.equals(uid, key)) {
                continue;
            }
            //得到用户所有的浏览记录
            stringList = browseRecordDao.selectNums(key, 30);
            otherImgDetailList = baseDao.selectBatchIds(stringList);

            for (ImgDetailsEntity e : otherImgDetailList) {
                if (!imgDetailList.contains(e) && !res.contains(e)) {
                    res.add(e);
                }
            }
        }

        //返回前20条数据(如果刷新则重新计算)
        return getImgDetailVoPage((int) page, (int) limit, res);
    }

    /**
     * 根据文本相似性计算
     *
     * @param page
     * @param limit
     * @param uid
     * @return
     */
    @Override
    public Page<ImgDetailVo> recommendToUser2(long page, long limit, String uid) {
        String ukey = "brimg:" + uid;
        //这里获取当前用户的前20条浏览记录
        Set<String> mids = redisUtils.zReverseRange(ukey, 0, 20);

        List<ImgDetailsEntity> imgDetailList = baseDao.selectBatchIds(mids);

        //创建文本
        StringBuilder sourceBuilder = new StringBuilder();
        for (ImgDetailsEntity model : imgDetailList) {
            sourceBuilder.append(model.getContent());
            sourceBuilder.append(";");
        }

        RecommendUtils mySimHash1 = new RecommendUtils(sourceBuilder.toString(), 64);

        //随机从所有的图片数据库中选择100条数据
        List<ImgDetailsEntity> imgDetailsEntityList = baseDao.selectRand(100);

        //计算相似性
        Map<ImgDetailsEntity, Double> map = new HashMap<>();

        for (ImgDetailsEntity model : imgDetailsEntityList) {
            RecommendUtils mySimHash2 = new RecommendUtils(model.getContent(), 64);
            Double similar = mySimHash1.getSimilar(mySimHash2);
            map.put(model, similar);
        }


        Map<ImgDetailsEntity, Double> map2 = map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldVal, newVal) -> oldVal,
                        LinkedHashMap::new));

        List<ImgDetailsEntity> list = new ArrayList<>();
        for (ImgDetailsEntity key : map2.keySet()) {
            if (!imgDetailList.contains(key)) {
                list.add(key);
            }
        }


        return getImgDetailVoPage((int) page, (int) limit, list);
    }

    @NotNull
    private Page<ImgDetailVo> getImgDetailVoPage(int page, int limit, List<ImgDetailsEntity> list) {
        List<ImgDetailVo> resultList = new ArrayList<>();
        ImgDetailVo imgDetailVo =null;
        List<String> imgList = null;
        UserEntity user = null;
        for (ImgDetailsEntity model : list) {
            imgDetailVo = ConvertUtils.sourceToTarget(model, ImgDetailVo.class);
            imgList = JSON.parseArray(model.getImgsUrl(), String.class);
            imgDetailVo.setImgsUrl(imgList);
            user = userDao.selectOne(new QueryWrapper<UserEntity>().eq("id", model.getUserId()));
            imgDetailVo.setUserId(user.getId());
            imgDetailVo.setUsername(user.getUsername());
            imgDetailVo.setAvatar(user.getAvatar());
            resultList.add(imgDetailVo);
        }

        return PageUtils.getPages(page, limit, resultList);
    }
}