package com.xz.recommend.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xz.common.utils.Result;
import com.xz.recommend.common.client.RecommendClient;
import com.xz.recommend.common.redis.RedisUtils;
import com.xz.recommend.service.ImgDetailsService;
import com.xz.recommend.vo.ImgDetailVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("recommend")
@Api(tags="推送模块")
public class ImgDetailsController {


    @Autowired
    ImgDetailsService imgDetailsService;

    @Autowired
    RedisUtils redisUtils;

    @Autowired
    RecommendClient recommendClient;


    @RequestMapping("recommendToUser/{page}/{limit}")
    public Result<?> recommendToUser(@PathVariable long page, @PathVariable long limit, String uid) {
        Page<ImgDetailVo> pageInfo = imgDetailsService.recommendToUser(page, limit, uid);
        return new Result<Page<ImgDetailVo>>().ok(pageInfo);
    }


    @RequestMapping("recommendToUser2/{page}/{limit}")
    public Result<?> recommendToUser2(@PathVariable long page, @PathVariable long limit, String uid) {

        String ukey = "brimg:" + uid;
        //这里获取当前用户的前20条浏览记录
        Set<String> mids = redisUtils.zReverseRange(ukey, 0, 20);

        if (mids.isEmpty()) {
            return recommendClient.getPage(page, limit);
        }

        Page<ImgDetailVo> pageInfo = imgDetailsService.recommendToUser2(page, limit, uid);
        return new Result<Page<ImgDetailVo>>().ok(pageInfo);
    }

}
