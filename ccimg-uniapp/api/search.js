import { request } from '../utils/request.js'
import { appConfig } from '../config/config.js'



export function esSearch (page,limit,params) {
  return request.get(appConfig.WEB_API + `/search/search/esSearch/${page}/${limit}`, params)  
}