/*
Navicat MySQL Data Transfer

Source Server         : node3.js.giao.me
Source Server Version : 80022
Source Host           : node3.js.giao.me:9029
Source Database       : ccimg-platform

Target Server Type    : MYSQL
Target Server Version : 80022
File Encoding         : 65001

Date: 2023-04-17 13:20:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_agree
-- ----------------------------
DROP TABLE IF EXISTS `t_agree`;
CREATE TABLE `t_agree` (
  `id` bigint NOT NULL,
  `uid` bigint DEFAULT NULL,
  `agree_id` bigint DEFAULT NULL,
  `agree_uid` bigint DEFAULT NULL,
  `type` int DEFAULT NULL,
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_agree
-- ----------------------------
INSERT INTO `t_agree` VALUES ('1647828350322380801', '1601126546037874690', '1647566765456646146', '1601126546037874690', '1', '111111', '2023-04-17 13:04:29');
INSERT INTO `t_agree` VALUES ('1647828387207090178', '1601126546037874690', '1645802812481245185', '1601126546037874692', '1', '111111', '2023-04-17 13:04:37');
INSERT INTO `t_agree` VALUES ('1647828517075324930', '1601126546037874692', '1647828510314106881', '1601126546037874692', '0', '111111', '2023-04-17 13:05:08');
INSERT INTO `t_agree` VALUES ('1647828520795672577', '1601126546037874692', '1647828397281808385', '1601126546037874690', '0', '111111', '2023-04-17 13:05:09');
INSERT INTO `t_agree` VALUES ('1647829602972241922', '1601126546037874692', '1647829566515351553', '1601126546037874692', '0', '111111', '2023-04-17 13:09:27');

-- ----------------------------
-- Table structure for t_album
-- ----------------------------
DROP TABLE IF EXISTS `t_album`;
CREATE TABLE `t_album` (
  `id` bigint NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `uid` bigint DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `img_count` bigint DEFAULT '0',
  `collection_count` bigint DEFAULT '0',
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `updater` bigint DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_album
-- ----------------------------
INSERT INTO `t_album` VALUES ('1640610664621629442', '蜡笔小新', '1599618448034959361', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/17109e93b4e145faa9f7fdc82851c1441679667713-origin-IMG_2049.PNG', null, '29', '1', '111111', '2023-03-28 15:03:58', null, '2023-03-28 15:04:20');
INSERT INTO `t_album` VALUES ('1640610976237445121', '猫咪', '1599618448034959361', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/21114bdc067349a5b6e353b9e520608f1679673796-origin-IMG_2072.JPG', null, '5', '0', '111111', '2023-03-28 15:05:12', null, '2023-03-28 15:05:31');
INSERT INTO `t_album` VALUES ('1640611875043237889', '动漫', '1599618448034959361', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/581d68107c544d078059901aa7034a271679665832-origin-IMG_1773.PNG', null, '14', '0', '111111', '2023-03-28 15:08:47', null, '2023-03-28 15:10:06');
INSERT INTO `t_album` VALUES ('1640612857319874562', '柯南', '1599618448034959361', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/815c01b5cca54533a2ce81a17014dded1679665969-origin-IMG_1780.PNG', null, '9', '0', '111111', '2023-03-28 15:12:41', null, '2023-03-28 15:13:12');
INSERT INTO `t_album` VALUES ('1640613641692471298', '小埋', '1599618448034959361', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/74a5a1777b164518803d11f66dadd9bd1679666141-origin-IMG_1796.PNG', null, '9', '0', '111111', '2023-03-28 15:15:48', null, '2023-03-28 15:16:12');
INSERT INTO `t_album` VALUES ('1640613986824970241', '猫咪', '1601126546037874692', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/a3dd7d3f5fe54072b7944ff349f0a4501679673917-origin-IMG_2102.JPG', null, '13', '0', '111111', '2023-03-28 15:17:10', null, '2023-03-28 15:17:29');
INSERT INTO `t_album` VALUES ('1640614541630726146', '风景', '1601126546037874692', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e6067ac239054f8090bae417d46d0c8e1679643053-origin-IMG_1735.PNG', null, '21', '0', '111111', '2023-03-28 15:19:23', null, '2023-03-28 15:20:00');
INSERT INTO `t_album` VALUES ('1640616086380306434', '花', '1601126546037874692', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/92cbef5bc5c54eeb9aa97480480508d21679666373-origin-IMG_1840.JPG', null, '18', '0', '111111', '2023-03-28 15:25:31', null, '2023-03-28 15:25:50');
INSERT INTO `t_album` VALUES ('1640616667614371842', '卡通壁纸', '1601126546037874690', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/bad76156ffc64eb3a809e906b06dc0281679666459-origin-IMG_1965.PNG', null, '28', '0', '111111', '2023-03-28 15:27:49', null, '2023-03-28 15:28:18');
INSERT INTO `t_album` VALUES ('1640616965187657729', '壁纸', '1601126546037874690', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/be5f5e46c6bc4b49bcd3e0d45985d6281679666467-origin-IMG_1978.PNG', null, '27', '0', '111111', '2023-03-28 15:29:00', null, '2023-03-28 15:29:17');
INSERT INTO `t_album` VALUES ('1640619364740587522', 'ins', '1601126546037874690', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/0ac1bd5270c44d9db31dd0071a4207481679666391-origin-IMG_1884.JPG', null, '7', '0', '111111', '2023-03-28 15:38:32', null, '2023-03-28 15:38:58');
INSERT INTO `t_album` VALUES ('1640621892681162754', '蜡笔小新', '1601126546037874690', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/474b7de919d64576bef5e9ab424889d51679667640-origin-IMG_2043.PNG', null, '11', '1', '111111', '2023-03-28 15:48:35', null, '2023-03-28 15:53:18');
INSERT INTO `t_album` VALUES ('1640627872496201729', '好看', '1601126546037874693', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/fa5875df487c448696772966b90cb3ad1679666384-origin-IMG_1852.JPG', null, '12', '0', '111111', '2023-03-28 16:12:21', null, '2023-03-28 16:12:43');
INSERT INTO `t_album` VALUES ('1640628156224090114', 'ins', '1601126546037874693', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/3753c1795ce94b71b4a64e3187d5e5c31679664800-origin-IMG_1760.JPG', null, '22', '0', '111111', '2023-03-28 16:13:28', null, '2023-03-28 16:13:58');
INSERT INTO `t_album` VALUES ('1640629134096707585', '蜡笔小新', '1601126546037874693', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/ecb8fc153bc1443fbe1cd827f6eceff31679667790-origin-IMG_2059.PNG', null, '15', '2', '111111', '2023-03-28 16:17:22', null, '2023-03-28 16:17:46');
INSERT INTO `t_album` VALUES ('1640630433064267778', '花', '1601126546037874691', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4d3182ce39204d8fa9349d7ed17b9fedduitang_1679575859681.png', null, '5', '0', '111111', '2023-03-28 16:22:31', null, '2023-03-28 16:22:52');
INSERT INTO `t_album` VALUES ('1640630664728260610', 'ins风格', '1601126546037874691', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/11/a166bbfd40544bf0a3bda301a0c76feb20230129124200_db71a.jpg', null, '7', '1', '111111', '2023-03-28 16:23:27', null, '2023-04-11 18:51:23');
INSERT INTO `t_album` VALUES ('1640631405366849537', '默认专辑', '1599618448034959361', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/643de2a7a33a4c94adbd70689e70ed9bduitang_1679575803319.png', null, '23', '3', '111111', '2023-03-28 16:26:23', null, '2023-03-28 16:26:23');
INSERT INTO `t_album` VALUES ('1640959125607792642', '风景', '1599618448034959361', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/29/4b7ed63b41ee4439b1f8082ae27bcf98duitang_1679575878181.png', null, '9', '1', '111111', '2023-03-29 14:08:38', null, '2023-03-29 14:10:01');
INSERT INTO `t_album` VALUES ('1645003396602171394', '蜡笔小新', '1601126546037874692', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/c3de09b7598c456287f574cfbab360ad20220615164102_ea3cb.jpeg', null, '14', '2', '111111', '2023-04-09 17:59:07', null, '2023-04-10 07:58:46');
INSERT INTO `t_album` VALUES ('1645431274976370690', '风景', '1601126546037874690', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/0e0f21fda74842358866d2c6187e927820230129124200_db71a.jpg', null, '4', '1', '111111', '2023-04-10 22:19:21', null, '2023-04-10 22:19:47');
INSERT INTO `t_album` VALUES ('1646877845903470593', '你好世界', '1646790471022325762', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/14/aae475c2b92e4f73a099d6edafd7e911wx_camera_1681480750698.jpg', null, '0', '0', '111111', '2023-04-14 22:07:31', null, '2023-04-14 22:07:31');
INSERT INTO `t_album` VALUES ('1646902044437082114', '我的', '1646901667952111618', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/20/4bae43e216124cf1966a474594a81612img_nodata.png', null, '0', '0', '111111', '2023-04-14 23:43:40', null, '2023-04-14 23:43:40');
INSERT INTO `t_album` VALUES ('1646902088754098177', '我的', '1646901667952111618', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/20/4bae43e216124cf1966a474594a81612img_nodata.png', null, '0', '0', '111111', '2023-04-14 23:43:51', null, '2023-04-14 23:43:51');
INSERT INTO `t_album` VALUES ('1646902633984258049', '你好', '1646901667952111618', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/14/22b733c9acf24c198166f8e191a13adewx_camera_1681480750698.jpg', null, '0', '0', '111111', '2023-04-14 23:46:01', null, '2023-04-14 23:46:01');
INSERT INTO `t_album` VALUES ('1646903197811961858', '风景', '1646901667952111618', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/20/4bae43e216124cf1966a474594a81612img_nodata.png', null, '0', '0', '111111', '2023-04-14 23:48:15', null, '2023-04-14 23:48:15');
INSERT INTO `t_album` VALUES ('1646903269006077953', '风景', '1646901667952111618', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/20/4bae43e216124cf1966a474594a81612img_nodata.png', null, '0', '0', '111111', '2023-04-14 23:48:32', null, '2023-04-14 23:48:32');
INSERT INTO `t_album` VALUES ('1646904537011286017', '生活', '1646901667952111618', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/20/4bae43e216124cf1966a474594a81612img_nodata.png', null, '0', '0', '111111', '2023-04-14 23:53:34', null, '2023-04-14 23:53:34');
INSERT INTO `t_album` VALUES ('1646904590702571521', '生活', '1646901667952111618', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/20/4bae43e216124cf1966a474594a81612img_nodata.png', null, '0', '0', '111111', '2023-04-14 23:53:47', null, '2023-04-14 23:53:47');

-- ----------------------------
-- Table structure for t_album_img_relation
-- ----------------------------
DROP TABLE IF EXISTS `t_album_img_relation`;
CREATE TABLE `t_album_img_relation` (
  `id` bigint NOT NULL,
  `aid` bigint DEFAULT NULL,
  `img_path` varchar(255) DEFAULT NULL,
  `mid` bigint DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `updater` bigint DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_album_img_relation
-- ----------------------------
INSERT INTO `t_album_img_relation` VALUES ('1640610678102122498', '1640610664621629442', null, '1640610678102122497', '0', '111111', '2023-03-28 15:04:01', null, '2023-03-28 15:04:01');
INSERT INTO `t_album_img_relation` VALUES ('1640610989902487554', '1640610976237445121', null, '1640610989889904642', '0', '111111', '2023-03-28 15:05:16', null, '2023-03-28 15:05:16');
INSERT INTO `t_album_img_relation` VALUES ('1640611505302757378', '1640610664621629442', null, '1640611505290174465', '0', '111111', '2023-03-28 15:07:19', null, '2023-03-28 15:07:19');
INSERT INTO `t_album_img_relation` VALUES ('1640611666812821507', '1640610664621629442', null, '1640611666812821506', '0', '111111', '2023-03-28 15:07:57', null, '2023-03-28 15:07:57');
INSERT INTO `t_album_img_relation` VALUES ('1640611886250418178', '1640611875043237889', null, '1640611886250418177', '0', '111111', '2023-03-28 15:08:49', null, '2023-03-28 15:08:49');
INSERT INTO `t_album_img_relation` VALUES ('1640612106157776897', '1640611875043237889', null, '1640612106140999681', '0', '111111', '2023-03-28 15:09:42', null, '2023-03-28 15:09:42');
INSERT INTO `t_album_img_relation` VALUES ('1640612431702876163', '1640611875043237889', null, '1640612431702876162', '0', '111111', '2023-03-28 15:10:59', null, '2023-03-28 15:10:59');
INSERT INTO `t_album_img_relation` VALUES ('1640612631418855427', '1640611875043237889', null, '1640612631418855426', '0', '111111', '2023-03-28 15:11:47', null, '2023-03-28 15:11:47');
INSERT INTO `t_album_img_relation` VALUES ('1640612907571830786', '1640612857319874562', null, '1640612907571830785', '0', '111111', '2023-03-28 15:12:53', null, '2023-03-28 15:12:53');
INSERT INTO `t_album_img_relation` VALUES ('1640613218139070466', '1640610664621629442', null, '1640613218122293250', '0', '111111', '2023-03-28 15:14:07', null, '2023-03-28 15:14:07');
INSERT INTO `t_album_img_relation` VALUES ('1640613484867444737', '1640612857319874562', null, '1640613484850667521', '0', '111111', '2023-03-28 15:15:11', null, '2023-03-28 15:15:11');
INSERT INTO `t_album_img_relation` VALUES ('1640613663507046402', '1640613641692471298', null, '1640613663507046401', '0', '111111', '2023-03-28 15:15:53', null, '2023-03-28 15:15:53');
INSERT INTO `t_album_img_relation` VALUES ('1640613999403687937', '1640613986824970241', null, '1640613999386910721', '0', '111111', '2023-03-28 15:17:13', null, '2023-03-28 15:17:13');
INSERT INTO `t_album_img_relation` VALUES ('1640614260952096769', '1640613986824970241', null, '1640614260939513858', '0', '111111', '2023-03-28 15:18:16', null, '2023-03-28 15:18:16');
INSERT INTO `t_album_img_relation` VALUES ('1640614593845616643', '1640614541630726146', null, '1640614593845616642', '0', '111111', '2023-03-28 15:19:35', null, '2023-03-28 15:19:35');
INSERT INTO `t_album_img_relation` VALUES ('1640614914206556162', '1640614541630726146', null, '1640614914206556161', '0', '111111', '2023-03-28 15:20:51', null, '2023-03-28 15:20:51');
INSERT INTO `t_album_img_relation` VALUES ('1640615323520294913', '1640614541630726146', null, '1640615323503517697', '0', '111111', '2023-03-28 15:22:29', null, '2023-03-28 15:22:29');
INSERT INTO `t_album_img_relation` VALUES ('1640615544312651778', '1640614541630726146', null, '1640615544300068866', '0', '111111', '2023-03-28 15:23:22', null, '2023-03-28 15:23:22');
INSERT INTO `t_album_img_relation` VALUES ('1640615698063253505', '1640614541630726146', null, '1640615698050670593', '0', '111111', '2023-03-28 15:23:58', null, '2023-03-28 15:23:58');
INSERT INTO `t_album_img_relation` VALUES ('1640615873733287938', '1640614541630726146', null, '1640615873733287937', '0', '111111', '2023-03-28 15:24:40', null, '2023-03-28 15:24:40');
INSERT INTO `t_album_img_relation` VALUES ('1640616099688833025', '1640616086380306434', null, '1640616099672055809', '0', '111111', '2023-03-28 15:25:34', null, '2023-03-28 15:25:34');
INSERT INTO `t_album_img_relation` VALUES ('1640616384800841729', '1640616086380306434', null, '1640616384784064514', '0', '111111', '2023-03-28 15:26:42', null, '2023-03-28 15:26:42');
INSERT INTO `t_album_img_relation` VALUES ('1640616679912071169', '1640616667614371842', null, '1640616679895293953', '0', '111111', '2023-03-28 15:27:52', null, '2023-03-28 15:27:52');
INSERT INTO `t_album_img_relation` VALUES ('1640616974083776514', '1640616965187657729', null, '1640616974083776513', '0', '111111', '2023-03-28 15:29:02', null, '2023-03-28 15:29:02');
INSERT INTO `t_album_img_relation` VALUES ('1640617201301807107', '1640616667614371842', null, '1640617201301807106', '0', '111111', '2023-03-28 15:29:57', null, '2023-03-28 15:29:57');
INSERT INTO `t_album_img_relation` VALUES ('1640617316422868993', '1640616667614371842', null, '1640617316410286081', '0', '111111', '2023-03-28 15:30:24', null, '2023-03-28 15:30:24');
INSERT INTO `t_album_img_relation` VALUES ('1640617560803991553', '1640616965187657729', null, '1640617560791408641', '0', '111111', '2023-03-28 15:31:22', null, '2023-03-28 15:31:22');
INSERT INTO `t_album_img_relation` VALUES ('1640617780686184450', '1640616965187657729', null, '1640617780686184449', '0', '111111', '2023-03-28 15:32:15', null, '2023-03-28 15:32:15');
INSERT INTO `t_album_img_relation` VALUES ('1640619375431868418', '1640619364740587522', null, '1640619375415091202', '0', '111111', '2023-03-28 15:38:35', null, '2023-03-28 15:38:35');
INSERT INTO `t_album_img_relation` VALUES ('1640621906459451394', '1640621892681162754', null, '1640621906459451393', '0', '111111', '2023-03-28 15:48:38', null, '2023-03-28 15:48:38');
INSERT INTO `t_album_img_relation` VALUES ('1640627885532098563', '1640627872496201729', null, '1640627885532098562', '0', '111111', '2023-03-28 16:12:24', null, '2023-03-28 16:12:24');
INSERT INTO `t_album_img_relation` VALUES ('1640628168987357186', '1640628156224090114', null, '1640628168987357185', '0', '111111', '2023-03-28 16:13:32', null, '2023-03-28 16:13:32');
INSERT INTO `t_album_img_relation` VALUES ('1640628406464655363', '1640628156224090114', null, '1640628406464655362', '0', '111111', '2023-03-28 16:14:28', null, '2023-03-28 16:14:28');
INSERT INTO `t_album_img_relation` VALUES ('1640628584332505089', '1640628156224090114', null, '1640628584319922177', '0', '111111', '2023-03-28 16:15:11', null, '2023-03-28 16:15:11');
INSERT INTO `t_album_img_relation` VALUES ('1640629142246240258', '1640629134096707585', null, '1640613218122293250', null, '111111', '2023-03-28 16:17:24', null, '2023-03-28 16:17:24');
INSERT INTO `t_album_img_relation` VALUES ('1640630462780911618', '1640630433064267778', null, '1640630462768328706', '0', '111111', '2023-03-28 16:22:38', null, '2023-03-28 16:22:38');
INSERT INTO `t_album_img_relation` VALUES ('1640630672840044546', '1640630664728260610', null, '1640619375415091202', null, '111111', '2023-03-28 16:23:29', null, '2023-03-28 16:23:29');
INSERT INTO `t_album_img_relation` VALUES ('1640959150815559682', '1640610664621629442', null, '1640959150773616642', '0', '111111', '2023-03-29 14:08:44', null, '2023-03-29 14:08:44');
INSERT INTO `t_album_img_relation` VALUES ('1640959616274251777', '1640631405366849537', null, '1640628406464655362', null, '111111', '2023-03-29 14:10:35', null, '2023-03-29 14:10:35');
INSERT INTO `t_album_img_relation` VALUES ('1642904579093643266', '1640621892681162754', null, '1640959150773616642', null, '111111', '2023-04-03 22:59:10', null, '2023-04-03 22:59:10');
INSERT INTO `t_album_img_relation` VALUES ('1644887101717463042', '1640631405366849537', null, '1640617560791408641', null, '111111', '2023-04-09 10:17:00', null, '2023-04-09 10:17:00');
INSERT INTO `t_album_img_relation` VALUES ('1644887671178117121', '1640631405366849537', null, '1640616679895293953', null, '111111', '2023-04-09 10:19:16', null, '2023-04-09 10:19:16');
INSERT INTO `t_album_img_relation` VALUES ('1644902410034466817', '1640959125607792642', null, '1640615323503517697', null, '111111', '2023-04-09 11:17:50', null, '2023-04-09 11:17:50');
INSERT INTO `t_album_img_relation` VALUES ('1644990182262870018', '1640610664621629442', null, '1644990182220926977', '0', '111111', '2023-04-09 17:06:37', null, '2023-04-09 17:06:37');
INSERT INTO `t_album_img_relation` VALUES ('1645003419385630722', '1645003396602171394', null, '1640626471053406210', null, '111111', '2023-04-09 17:59:13', null, '2023-04-09 17:59:13');
INSERT INTO `t_album_img_relation` VALUES ('1645340754031710211', '1645003396602171394', null, '1645340754031710210', '0', '111111', '2023-04-10 16:19:39', null, '2023-04-10 16:19:39');
INSERT INTO `t_album_img_relation` VALUES ('1645423135417753602', '1640621892681162754', null, '1640626471053406210', '0', '111111', '2023-04-10 21:47:01', null, '2023-04-10 21:47:01');
INSERT INTO `t_album_img_relation` VALUES ('1645431306022608898', '1645431274976370690', null, '1645431306005831682', '0', '111111', '2023-04-10 22:19:29', null, '2023-04-10 22:19:29');
INSERT INTO `t_album_img_relation` VALUES ('1645432934213672962', '1640629134096707585', null, '1645432934201090050', '0', '111111', '2023-04-10 22:25:57', null, '2023-04-10 22:25:57');
INSERT INTO `t_album_img_relation` VALUES ('1645453878030295041', '1640627872496201729', null, '1645453878013517826', '0', '111111', '2023-04-10 23:49:10', null, '2023-04-10 23:49:10');
INSERT INTO `t_album_img_relation` VALUES ('1645635011338604545', '1645003396602171394', null, '1645635011271495681', '0', '111111', '2023-04-11 11:48:56', null, '2023-04-11 11:48:56');
INSERT INTO `t_album_img_relation` VALUES ('1645802812489633794', '1640614541630726146', null, '1645802812481245185', '0', '111111', '2023-04-11 22:55:43', null, '2023-04-11 22:55:43');
INSERT INTO `t_album_img_relation` VALUES ('1645802821096345602', '1640616965187657729', null, '1645802821087956993', '0', '111111', '2023-04-11 22:55:45', null, '2023-04-11 22:55:45');
INSERT INTO `t_album_img_relation` VALUES ('1646116953272168449', '1645003396602171394', null, '1640621906459451393', null, '111111', '2023-04-12 19:44:00', null, '2023-04-12 19:44:00');
INSERT INTO `t_album_img_relation` VALUES ('1646117102836854786', '1645431274976370690', null, '1645802812481245185', null, '111111', '2023-04-12 19:44:35', null, '2023-04-12 19:44:35');
INSERT INTO `t_album_img_relation` VALUES ('1647564622108581890', '1640610664621629442', null, '1640626471053406210', null, '111111', '2023-04-16 19:36:31', null, '2023-04-16 19:36:31');
INSERT INTO `t_album_img_relation` VALUES ('1647566765456646147', '1640616667614371842', null, '1647566765456646146', '0', '111111', '2023-04-16 19:45:02', null, '2023-04-16 19:45:02');

-- ----------------------------
-- Table structure for t_browse_record
-- ----------------------------
DROP TABLE IF EXISTS `t_browse_record`;
CREATE TABLE `t_browse_record` (
  `id` bigint NOT NULL,
  `uid` bigint DEFAULT NULL,
  `mid` bigint DEFAULT NULL,
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `updater` bigint DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_browse_record
-- ----------------------------
INSERT INTO `t_browse_record` VALUES ('1647828317611003906', '1601126546037874690', '1647566765456646146', '111111', '2023-04-17 13:04:21', null, '2023-04-17 13:06:44');
INSERT INTO `t_browse_record` VALUES ('1647828367934263298', '1601126546037874690', '1640617560791408641', '111111', '2023-04-17 13:04:33', null, '2023-04-17 13:04:33');
INSERT INTO `t_browse_record` VALUES ('1647828382408806402', '1601126546037874690', '1645802812481245185', '111111', '2023-04-17 13:04:36', null, '2023-04-17 13:05:39');
INSERT INTO `t_browse_record` VALUES ('1647828454190125057', '1601126546037874692', '1645802812481245185', '111111', '2023-04-17 13:04:53', null, '2023-04-17 13:05:23');
INSERT INTO `t_browse_record` VALUES ('1647828670184198146', '1601126546037874690', '1645802821087956993', '111111', '2023-04-17 13:05:45', null, '2023-04-17 13:05:45');
INSERT INTO `t_browse_record` VALUES ('1647829135038910465', '1601126546037874690', '1640614593845616642', '111111', '2023-04-17 13:07:36', null, '2023-04-17 13:08:08');
INSERT INTO `t_browse_record` VALUES ('1647829264600961026', '1601126546037874690', '1645453878013517826', '111111', '2023-04-17 13:08:07', null, '2023-04-17 13:08:07');
INSERT INTO `t_browse_record` VALUES ('1647829389779963906', '1601126546037874692', '1640614593845616642', '111111', '2023-04-17 13:08:36', null, '2023-04-17 13:09:30');

-- ----------------------------
-- Table structure for t_category
-- ----------------------------
DROP TABLE IF EXISTS `t_category`;
CREATE TABLE `t_category` (
  `id` bigint NOT NULL,
  `name` varchar(50) NOT NULL,
  `pid` bigint DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `count` bigint DEFAULT NULL,
  `description` longtext,
  `cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分类的封面，如果是一级分类就是随便看看的封面，二级分类则是主封面',
  `hot_cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '热门封面',
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `updater` bigint DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_category
-- ----------------------------
INSERT INTO `t_category` VALUES ('1594985167723864061', '头像', '0', null, null, null, 'https://img1.baidu.com/it/u=4048071112,566005454&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', 'https://img2.baidu.com/it/u=453253244,3693084626&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', null, '2023-03-24 14:52:52', null, '2023-03-24 14:53:15');
INSERT INTO `t_category` VALUES ('1594985167723864062', '壁纸', '0', null, null, null, 'https://img0.baidu.com/it/u=1275095085,1961143463&fm=253&fmt=auto&app=120&f=JPEG?w=1280&h=800', 'https://img2.baidu.com/it/u=40095075,1211536451&fm=253&fmt=auto&app=120&f=JPEG?w=1280&h=800', null, '2023-03-24 14:52:54', null, '2023-03-24 14:53:17');
INSERT INTO `t_category` VALUES ('1594985167723864063', '表情', '0', null, null, null, 'https://preview.qiantucdn.com/58pic/34/86/20/76W58PIC8IX38it8958PICefA_PIC2018.gif%21w1024_new_small', 'https://img2.baidu.com/it/u=754933101,1986163827&fm=253&fmt=auto&app=120&f=JPEG?w=938&h=800', null, '2023-03-24 14:52:56', null, '2023-03-24 14:53:20');
INSERT INTO `t_category` VALUES ('1594985167723864064', '主题', '0', null, null, null, 'https://img0.baidu.com/it/u=2061015773,2041399178&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=281', 'https://img2.baidu.com/it/u=826772970,730108778&fm=253&fmt=auto&app=120&f=JPEG?w=1280&h=800', null, '2023-03-24 14:52:58', null, '2023-03-24 14:53:22');
INSERT INTO `t_category` VALUES ('1594985167723864065', '影视', '0', null, null, null, 'https://img1.baidu.com/it/u=1465299912,3081968049&fm=253&fmt=auto&app=120&f=JPEG?w=1200&h=675', 'https://img2.baidu.com/it/u=1390720635,1469917720&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=313', null, '2023-03-24 14:53:00', null, '2023-03-24 14:53:25');
INSERT INTO `t_category` VALUES ('1594985167723864066', '动漫', '0', null, null, null, 'https://img2.baidu.com/it/u=3557507037,4178417346&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=1029', 'https://img0.baidu.com/it/u=3956925934,1485967994&fm=253&fmt=auto&app=138&f=JPEG?w=889&h=500', null, '2023-03-24 14:53:03', null, '2023-03-24 14:53:27');
INSERT INTO `t_category` VALUES ('1594985167723864067', '萌宠', '0', null, null, null, 'https://img0.baidu.com/it/u=2017189832,3005462452&fm=253&fmt=auto&app=138&f=JPEG?w=507&h=500', 'https://img1.baidu.com/it/u=392440737,274899463&fm=253&fmt=auto&app=138&f=JPEG?w=756&h=500', null, '2023-03-24 14:53:05', null, '2023-03-24 14:53:29');
INSERT INTO `t_category` VALUES ('1594985167723864068', '绘画', '0', null, null, null, 'https://img0.baidu.com/it/u=3606675823,19392459&fm=253&fmt=auto&app=138&f=JPEG?w=614&h=500', 'https://img1.baidu.com/it/u=2139756579,363196934&fm=253&fmt=auto?w=950&h=544', null, '2023-03-24 14:53:07', null, '2023-03-24 14:53:32');
INSERT INTO `t_category` VALUES ('1594985167723864069', '美食', '0', null, null, null, 'https://img2.baidu.com/it/u=3198551379,2075192309&fm=253&fmt=auto&app=120&f=JPEG?w=1422&h=800', 'https://img2.baidu.com/it/u=1688025837,843491578&fm=253&fmt=auto&app=120&f=JPEG?w=889&h=500', null, '2023-03-24 14:53:10', null, '2023-03-24 14:53:34');
INSERT INTO `t_category` VALUES ('1594985167723864110', '风景', '0', null, null, null, 'https://img1.baidu.com/it/u=3610922680,1058803729&fm=253&fmt=auto&app=138&f=JPEG?w=800&h=500', 'https://img2.baidu.com/it/u=355673106,1614830790&fm=253&fmt=auto&app=138&f=JPEG?w=800&h=500', null, '2023-03-24 14:53:12', null, '2023-03-24 14:53:36');
INSERT INTO `t_category` VALUES ('1594985167723864111', '动漫', '1594985167723864061', null, null, '动漫头像', 'https://img1.baidu.com/it/u=3849491138,950134800&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=820', null, null, '2023-03-24 15:07:07', null, '2023-03-24 15:07:10');
INSERT INTO `t_category` VALUES ('1594985167723864112', '女生头像', '1594985167723864061', null, null, '适合女孩子的头像', 'https://img2.baidu.com/it/u=1336099446,661546451&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', null, null, '2023-03-24 15:12:39', null, '2023-03-24 15:12:53');
INSERT INTO `t_category` VALUES ('1594985167723864113', '男生头像', '1594985167723864061', null, null, '适合男孩子的头像', 'https://img1.baidu.com/it/u=773188203,2981672265&fm=253&fmt=auto&app=138&f=JPEG?w=281&h=500', null, null, '2023-03-24 15:12:41', null, '2023-03-24 15:12:55');
INSERT INTO `t_category` VALUES ('1594985167723864114', '风景', '1594985167723864061', null, null, '好看的风景', 'https://img1.baidu.com/it/u=1681825099,2898845545&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=888', null, null, '2023-03-24 15:12:43', null, '2023-03-24 15:12:57');
INSERT INTO `t_category` VALUES ('1594985167723864115', '可爱', '1594985167723864061', null, null, '可爱的头像', 'https://img1.baidu.com/it/u=783236443,1559696047&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', null, null, '2023-03-24 15:12:45', null, '2023-03-24 15:13:00');
INSERT INTO `t_category` VALUES ('1594985167723864116', '情侣', '1594985167723864061', null, null, '情侣头像', 'https://img0.baidu.com/it/u=4168948101,1586552447&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=926', null, null, '2023-03-24 15:12:47', null, '2023-03-24 15:13:02');
INSERT INTO `t_category` VALUES ('1594985167723864117', '宠物', '1594985167723864061', null, null, '宠物头像', 'https://img0.baidu.com/it/u=3574495855,3620224580&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=888', null, null, '2023-03-24 15:12:49', null, '2023-03-24 15:13:04');
INSERT INTO `t_category` VALUES ('1594985167723864118', '卡通', '1594985167723864061', null, null, '卡通头像', 'https://img1.baidu.com/it/u=2785988489,2898142966&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', null, null, '2023-03-24 15:12:51', null, '2023-03-24 15:13:06');
INSERT INTO `t_category` VALUES ('1594985167723864119', '情侣', '1594985167723864062', null, null, '情侣壁纸', 'https://img0.baidu.com/it/u=1389592662,1064477027&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', null, null, '2023-03-24 15:18:48', null, '2023-03-24 15:19:08');
INSERT INTO `t_category` VALUES ('1594985167723864210', 'ins', '1594985167723864062', null, null, 'ins壁纸', 'https://img0.baidu.com/it/u=4114912361,4128907800&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889', null, null, '2023-03-24 15:18:51', null, '2023-03-24 15:19:10');
INSERT INTO `t_category` VALUES ('1594985167723864211', '文字', '1594985167723864062', null, null, '文字壁纸', 'https://img0.baidu.com/it/u=3872412964,297829627&fm=253&fmt=auto&app=138&f=JPEG?w=800&h=500', null, null, '2023-03-24 15:18:54', null, '2023-03-24 15:19:13');
INSERT INTO `t_category` VALUES ('1594985167723864212', '聊天', '1594985167723864062', null, null, '聊天壁纸', 'https://img2.baidu.com/it/u=3265236625,2572512370&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1679763600&t=a486c5f84d9daa84a0fec792bd2a7eb3', null, null, '2023-03-24 15:18:57', null, '2023-03-24 15:19:16');
INSERT INTO `t_category` VALUES ('1594985167723864215', '月亮', '1594985167723864062', null, null, '月亮壁纸', 'https://img1.baidu.com/it/u=709961623,2802134937&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1679763600&t=d517659230a4cf07b9d1fec9cac85f5a', null, null, '2023-03-24 15:18:58', null, '2023-03-24 15:19:17');
INSERT INTO `t_category` VALUES ('1594985167723864216', '星星', '1594985167723864062', null, null, '星星壁纸', 'https://img0.baidu.com/it/u=2710478475,729929474&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1679763600&t=6ade3194c88dced4e90b94353bc2ed0b', null, null, '2023-03-24 15:19:01', null, '2023-03-24 15:19:19');
INSERT INTO `t_category` VALUES ('1594985167723864217', '可爱', '1594985167723864062', null, null, '可爱壁纸', 'https://img1.baidu.com/it/u=3993474665,2175648748&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1679763600&t=e7fcf7182fc760b5b79a3f1866b91fc4', null, null, '2023-03-24 15:19:03', null, '2023-03-24 15:19:21');
INSERT INTO `t_category` VALUES ('1594985167723864218', '动漫', '1594985167723864062', null, null, '动漫壁纸', 'https://img2.baidu.com/it/u=262428766,2585383158&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1679763600&t=567b1a76131a1519fdc2e04d72762a7e', null, null, '2023-03-24 15:19:06', null, '2023-03-24 15:19:23');
INSERT INTO `t_category` VALUES ('1594985167723864310', '可爱', '1594985167723864063', null, null, '可爱的表情', 'https://img1.baidu.com/it/u=925635138,2683937048&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1679763600&t=cbb37481fff2c440c3720697c311ae45', null, null, '2023-03-24 15:24:33', null, '2023-03-24 15:24:49');
INSERT INTO `t_category` VALUES ('1594985167723864312', '搞笑', '1594985167723864063', null, null, '搞笑的表情', 'https://img0.baidu.com/it/u=1523344458,2815435434&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', null, null, '2023-03-24 15:24:36', null, '2023-03-24 15:24:53');
INSERT INTO `t_category` VALUES ('1594985167723864313', '动漫', '1594985167723864063', null, null, '动漫表情', 'https://img.soogif.com/pJTouuCsrb002iv3Tx8lpKGOSRx3YXbV.gif', null, null, '2023-03-24 15:24:38', null, '2023-03-24 15:24:55');
INSERT INTO `t_category` VALUES ('1594985167723864314', '无语', '1594985167723864063', null, null, '无语表情', 'https://img0.baidu.com/it/u=4218554309,3018209913&fm=253&fmt=auto&app=120&f=JPEG?w=800&h=800', null, null, '2023-03-24 15:24:40', null, '2023-03-24 15:24:57');
INSERT INTO `t_category` VALUES ('1594985167723864315', '猫猫', '1594985167723864063', null, null, '猫咪表情', 'https://p1.itc.cn/q_70/images03/20200722/79660b4538f54a0f929bb84bc90cb8f1.gif', null, null, '2023-03-24 15:24:42', null, '2023-03-24 15:24:59');
INSERT INTO `t_category` VALUES ('1594985167723864316', '汪星人', '1594985167723864063', null, null, '汪星人表情', 'https://img2.baidu.com/it/u=1039680708,13111231&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=667', null, null, '2023-03-24 15:24:43', null, '2023-03-24 15:25:02');
INSERT INTO `t_category` VALUES ('1594985167723864317', '打工人', '1594985167723864063', null, null, '打工人表情', 'https://img1.baidu.com/it/u=3634284552,1193261509&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=375', null, null, '2023-03-24 15:24:45', null, '2023-03-24 15:25:03');
INSERT INTO `t_category` VALUES ('1594985167723864318', '干饭', '1594985167723864063', null, null, '干饭表情', 'https://img1.baidu.com/it/u=224336331,2937738639&fm=253&fmt=auto&app=138&f=JPEG?w=470&h=500', null, null, '2023-03-24 15:24:47', null, '2023-03-24 15:25:05');
INSERT INTO `t_category` VALUES ('1594985167723864410', '柯南', '1594985167723864066', null, null, '柯南', 'https://img0.baidu.com/it/u=771098675,864035088&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1679763600&t=e2c973fb5708beb84d7ced7d2fdc72ba', null, null, '2023-03-24 22:46:42', null, '2023-03-24 22:46:56');
INSERT INTO `t_category` VALUES ('1594985167723864411', '火影', '1594985167723864066', null, null, '火影', 'https://img2.baidu.com/it/u=3329589379,2383322306&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1679763600&t=f49b515e917ce9fdfaee9abb31f84e1f', null, null, '2023-03-24 22:46:44', null, '2023-03-24 22:46:59');
INSERT INTO `t_category` VALUES ('1594985167723864412', '小埋', '1594985167723864066', null, null, '小埋', 'https://img0.baidu.com/it/u=3891227261,2556642340&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1679763600&t=8c2ed460b3a15ad4e81fd2c08a8e90d8', null, null, '2023-03-24 22:46:46', null, '2023-03-24 22:47:02');
INSERT INTO `t_category` VALUES ('1594985167723864413', '夏目友人帐', '1594985167723864066', null, null, '夏目友人帐', 'https://img2.baidu.com/it/u=2580604745,2746057038&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1679763600&t=359c987475d2be8d04d0d7bddf282642', null, null, '2023-03-24 22:46:47', null, '2023-03-24 22:47:03');
INSERT INTO `t_category` VALUES ('1594985167723864414', '海贼王', '1594985167723864066', null, null, '海贼王', 'https://img0.baidu.com/it/u=774421603,320974531&fm=253&fmt=auto&app=138&f=JPEG?w=889&h=500', null, null, '2023-03-24 22:46:49', null, '2023-03-24 22:47:08');
INSERT INTO `t_category` VALUES ('1594985167723864415', '蜡笔小新', '1594985167723864066', null, null, '蜡笔小新', 'https://img0.baidu.com/it/u=4198604161,3095630136&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889', null, null, '2023-03-24 22:46:50', null, '2023-03-24 22:47:30');
INSERT INTO `t_category` VALUES ('1594985167723864416', '堀与宫村', '1594985167723864066', null, null, '堀与宫村', 'https://img2.baidu.com/it/u=2497684617,3981443116&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', null, null, '2023-03-24 22:46:52', null, '2023-03-24 22:47:32');
INSERT INTO `t_category` VALUES ('1594985167723864417', '小林家的龙女仆', '1594985167723864066', null, null, '小林家的龙女仆', 'https://img2.baidu.com/it/u=3972068470,478085048&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', null, null, '2023-03-24 22:46:53', null, '2023-03-24 22:47:35');
INSERT INTO `t_category` VALUES ('1594985167723864510', '风景主题', '1594985167723864064', null, null, '好看的风景主题', 'https://img0.baidu.com/it/u=822449240,1181845398&fm=253&fmt=auto&app=120&f=JPEG?w=1422&h=800', null, null, '2023-04-16 21:25:19', null, '2023-04-16 21:25:21');
INSERT INTO `t_category` VALUES ('1594985167723864511', '情侣主题', '1594985167723864064', null, null, '情侣主题', 'https://img2.baidu.com/it/u=1674560603,1641379352&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889', null, null, '2023-04-16 21:31:39', null, '2023-04-16 21:31:41');
INSERT INTO `t_category` VALUES ('1594985167723864512', '新奇', '1594985167723864064', null, null, '新奇的主题', 'https://img0.baidu.com/it/u=1081045228,2520913741&fm=253&fmt=auto&app=138&f=JPEG?w=889&h=500', null, null, '2023-04-16 21:32:24', null, '2023-04-16 21:32:26');
INSERT INTO `t_category` VALUES ('1594985167723864513', '卡通', '1594985167723864064', null, null, '卡通主题', 'https://img0.baidu.com/it/u=3253491567,1083992946&fm=253&fmt=auto&app=138&f=JPG?w=281&h=500', null, null, '2023-04-16 21:34:35', null, '2023-04-16 21:34:37');

-- ----------------------------
-- Table structure for t_collection
-- ----------------------------
DROP TABLE IF EXISTS `t_collection`;
CREATE TABLE `t_collection` (
  `id` bigint NOT NULL,
  `uid` bigint DEFAULT NULL,
  `aid` bigint DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `updater` bigint DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_collection
-- ----------------------------

-- ----------------------------
-- Table structure for t_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_comment`;
CREATE TABLE `t_comment` (
  `id` bigint NOT NULL,
  `mid` bigint DEFAULT NULL,
  `uid` bigint DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pid` bigint DEFAULT NULL,
  `reply_id` bigint DEFAULT NULL,
  `reply_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `level` int DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `content` longtext,
  `count` bigint DEFAULT '0',
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_comment
-- ----------------------------
INSERT INTO `t_comment` VALUES ('1647828332479811585', '1647566765456646146', '1601126546037874690', 'qwer', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/b98f847362c447cf9c8a0d911dc6fe4e20220615164101_d77a8.jpeg', '0', '0', null, '1', null, '你好', '0', '111111', '2023-04-17 13:04:24');
INSERT INTO `t_comment` VALUES ('1647828397281808385', '1645802812481245185', '1601126546037874690', 'qwer', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/b98f847362c447cf9c8a0d911dc6fe4e20220615164101_d77a8.jpeg', '0', '0', null, '1', null, '你好', '1', '111111', '2023-04-17 13:04:40');
INSERT INTO `t_comment` VALUES ('1647828510314106881', '1645802812481245185', '1601126546037874692', 'ccz', 'https://img2.baidu.com/it/u=1196632544,116923444&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', '1647828397281808385', '1647828397281808385', 'qwer', '2', null, '你好啊', '1', '111111', '2023-04-17 13:05:07');
INSERT INTO `t_comment` VALUES ('1647829170304618497', '1640614593845616642', '1601126546037874690', 'qwer', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/b98f847362c447cf9c8a0d911dc6fe4e20220615164101_d77a8.jpeg', '0', '0', null, '1', null, '你好啊??', '0', '111111', '2023-04-17 13:07:44');
INSERT INTO `t_comment` VALUES ('1647829191955615745', '1640614593845616642', '1601126546037874690', 'qwer', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/b98f847362c447cf9c8a0d911dc6fe4e20220615164101_d77a8.jpeg', '1647829170304618497', '1647829170304618497', 'qwer', '2', null, '你是谁啊', '0', '111111', '2023-04-17 13:07:49');
INSERT INTO `t_comment` VALUES ('1647829212520288257', '1640614593845616642', '1601126546037874690', 'qwer', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/b98f847362c447cf9c8a0d911dc6fe4e20220615164101_d77a8.jpeg', '1647829170304618497', '1647829191955615745', 'qwer', '2', null, '你在干嘛呢', '0', '111111', '2023-04-17 13:07:54');
INSERT INTO `t_comment` VALUES ('1647829239854567425', '1640614593845616642', '1601126546037874690', 'qwer', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/b98f847362c447cf9c8a0d911dc6fe4e20220615164101_d77a8.jpeg', '1647829170304618497', '1647829212520288257', 'qwer', '2', null, '你好你好????', '0', '111111', '2023-04-17 13:08:01');
INSERT INTO `t_comment` VALUES ('1647829335052685313', '1640614593845616642', '1601126546037874690', 'qwer', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/b98f847362c447cf9c8a0d911dc6fe4e20220615164101_d77a8.jpeg', '0', '0', null, '1', null, '好的好的???会III????', '0', '111111', '2023-04-17 13:08:23');
INSERT INTO `t_comment` VALUES ('1647829422034161666', '1640614593845616642', '1601126546037874692', 'ccz', 'https://img2.baidu.com/it/u=1196632544,116923444&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', '1647829335052685313', '1647829335052685313', 'qwer', '2', null, '你是谁啊', '0', '111111', '2023-04-17 13:08:44');
INSERT INTO `t_comment` VALUES ('1647829481396146178', '1640614593845616642', '1601126546037874692', 'ccz', 'https://img2.baidu.com/it/u=1196632544,116923444&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', '1647829335052685313', '1647829422034161666', 'ccz', '2', null, '花钱????你好啊??', '0', '111111', '2023-04-17 13:08:58');
INSERT INTO `t_comment` VALUES ('1647829566515351553', '1640614593845616642', '1601126546037874692', 'ccz', 'https://img2.baidu.com/it/u=1196632544,116923444&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', '0', '0', null, '1', null, '??你在干嘛????', '1', '111111', '2023-04-17 13:09:19');
INSERT INTO `t_comment` VALUES ('1647829597230239746', '1640614593845616642', '1601126546037874692', 'ccz', 'https://img2.baidu.com/it/u=1196632544,116923444&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', '0', '0', null, '1', null, '为什么啊????', '0', '111111', '2023-04-17 13:09:26');
INSERT INTO `t_comment` VALUES ('1647829654113390593', '1640614593845616642', '1601126546037874692', 'ccz', 'https://img2.baidu.com/it/u=1196632544,116923444&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', '0', '0', null, '1', null, '牛ww', '0', '111111', '2023-04-17 13:09:39');
INSERT INTO `t_comment` VALUES ('1647829685025411074', '1640614593845616642', '1601126546037874692', 'ccz', 'https://img2.baidu.com/it/u=1196632544,116923444&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', '1647829654113390593', '1647829654113390593', 'ccz', '2', null, '好的好的???', '0', '111111', '2023-04-17 13:09:47');

-- ----------------------------
-- Table structure for t_follow
-- ----------------------------
DROP TABLE IF EXISTS `t_follow`;
CREATE TABLE `t_follow` (
  `id` bigint NOT NULL,
  `uid` bigint DEFAULT NULL,
  `fid` bigint DEFAULT NULL,
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_follow
-- ----------------------------

-- ----------------------------
-- Table structure for t_img_details
-- ----------------------------
DROP TABLE IF EXISTS `t_img_details`;
CREATE TABLE `t_img_details` (
  `id` bigint NOT NULL,
  `content` longtext,
  `cover` varchar(255) DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  `category_id` bigint DEFAULT NULL,
  `category_pid` bigint DEFAULT NULL,
  `imgs_url` longtext,
  `count` int DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `view_count` bigint DEFAULT '0',
  `agree_count` bigint DEFAULT '0',
  `collection_count` bigint DEFAULT '0',
  `comment_count` bigint DEFAULT '0',
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `updater` bigint DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_img_details
-- ----------------------------
INSERT INTO `t_img_details` VALUES ('1640610678102122497', '蜡笔小新壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/88ced5aef1fa4783bc6c1e5b11cb6eea1679667713-origin-IMG_2049.PNG', '1599618448034959361', '1594985167723864415', '1594985167723864066', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/88ced5aef1fa4783bc6c1e5b11cb6eea1679667713-origin-IMG_2049.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/db0a3bd608404a2bbb27a3a2829783cb1679667619-origin-IMG_2040.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/62af121fb2c34a3091b9cd036f59c02d1679667628-origin-IMG_2041.PNG\"]', '3', null, null, '16', '3', '1', '6', '111111', '2023-03-28 15:04:01', null, '2023-03-28 15:04:01');
INSERT INTO `t_img_details` VALUES ('1640610989889904642', '猫咪', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/7838c93aeacc455984a29c6ece21d6f51679673796-origin-IMG_2072.JPG', '1599618448034959361', '1594985167723864115', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/7838c93aeacc455984a29c6ece21d6f51679673796-origin-IMG_2072.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/21c8c4738e8d49cb8dbc8cbe5f5fa2021679673800-origin-IMG_2073.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/84fb24c61c5b4d029a812001bab28eac1679673803-origin-IMG_2074.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b320f7496f7e4be2a14c54a291b377fe1679673818-origin-IMG_2077.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/442dd6ad2f0f48a3b0cac650ef6bab451679673822-origin-IMG_2078.JPG\"]', '5', null, null, '1', '0', '0', '1', '111111', '2023-03-28 15:05:16', null, '2023-03-28 15:05:16');
INSERT INTO `t_img_details` VALUES ('1640611505290174465', '蜡笔小新头像壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/21a642dc1e6f45fe9dc45dc095cc059e1679667638-origin-IMG_2042.PNG', '1599618448034959361', '1594985167723864415', '1594985167723864066', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/21a642dc1e6f45fe9dc45dc095cc059e1679667638-origin-IMG_2042.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e84fcc2c4e0241b5867be6fe99f361bf1679667640-origin-IMG_2043.PNG\"]', '2', null, null, '31', '3', '0', '5', '111111', '2023-03-28 15:07:19', null, '2023-04-12 16:38:23');
INSERT INTO `t_img_details` VALUES ('1640611666812821506', '蜡笔小新壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/25dc025b85ac4af383b0620046082bb71679667717-origin-IMG_2050.PNG', '1599618448034959361', '1594985167723864415', '1594985167723864066', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/25dc025b85ac4af383b0620046082bb71679667717-origin-IMG_2050.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/cce89a071d7047ea972e4aa98a568fe11679667720-origin-IMG_2051.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e4794aebca18418cb9902aaa9dc2b04f1679667787-origin-IMG_2058.PNG\"]', '3', null, null, '13', '2', '0', '0', '111111', '2023-03-28 15:07:57', null, '2023-03-28 15:07:57');
INSERT INTO `t_img_details` VALUES ('1640611886250418177', '麻衣学姐', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/210335799b1d407aab10f6ec91ac70c91679665837-origin-IMG_1774.PNG', '1599618448034959361', '1594985167723864111', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/210335799b1d407aab10f6ec91ac70c91679665837-origin-IMG_1774.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/c712ce58bdfc4930850f448c2a1bd4d21679665832-origin-IMG_1773.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/c2cf59518583403c8ec2b4885ae190761679665855-origin-IMG_1775.PNG\"]', '3', null, null, '0', '0', '0', '0', '111111', '2023-03-28 15:08:49', null, '2023-03-28 15:08:49');
INSERT INTO `t_img_details` VALUES ('1640612106140999681', '火影忍者', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/d972de6450d3489688fec47fd37647401679666265-origin-IMG_1802.PNG', '1599618448034959361', '1594985167723864411', '1594985167723864066', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/d972de6450d3489688fec47fd37647401679666265-origin-IMG_1802.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5014856128614a159d3c32dcdd0cb31a1679666227-origin-IMG_1801.PNG\"]', '2', null, null, '0', '0', '0', '0', '111111', '2023-03-28 15:09:42', null, '2023-03-28 15:09:42');
INSERT INTO `t_img_details` VALUES ('1640612431702876162', '夏目', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/92ee94574fb247b1b66d1c3cc400d5da1679665905-origin-IMG_1776.PNG', '1599618448034959361', '1594985167723864413', '1594985167723864066', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/92ee94574fb247b1b66d1c3cc400d5da1679665905-origin-IMG_1776.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/a62dd2cdb3ba45abaeca559fe48b74bf1679665917-origin-IMG_1777.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5fbb1128bf9e4f92bad5d00ed53ff2591679665927-origin-IMG_1778.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/db9516a3a73c45df9d32099286cc0fec1679665936-origin-IMG_1779.PNG\"]', '4', null, null, '4', '1', '0', '0', '111111', '2023-03-28 15:10:59', null, '2023-03-28 15:10:59');
INSERT INTO `t_img_details` VALUES ('1640612631418855426', '小林家的龙女仆', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/d255c9e2b91247a3906e0f0178dcdedf1679665734-origin-IMG_1766.PNG', '1599618448034959361', '1594985167723864417', '1594985167723864066', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/d255c9e2b91247a3906e0f0178dcdedf1679665734-origin-IMG_1766.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e2877e49eddd44e48ed40c1edc2a4e7f1679665724-origin-IMG_1765.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e6db93774cb4434798efb16812aebf421679665766-origin-IMG_1769.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4db5fd39e6874f8c886d69d3a8e152711679665750-origin-IMG_1768.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/d3a8fe57675f42bd877f433da78ccee11679665742-origin-IMG_1767.PNG\"]', '5', null, null, '0', '0', '0', '0', '111111', '2023-03-28 15:11:47', null, '2023-03-28 15:11:47');
INSERT INTO `t_img_details` VALUES ('1640612907571830785', '新兰永恒', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/c8f0ac43e4ae46e99c416e33306795cf1679665969-origin-IMG_1780.PNG', '1599618448034959361', '1594985167723864410', '1594985167723864066', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/c8f0ac43e4ae46e99c416e33306795cf1679665969-origin-IMG_1780.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/93065d04d85d4cd4bb7acd775b8485d61679665978-origin-IMG_1781.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b4eb8b1085af42d4bcd9eb3d1454a77a1679665993-origin-IMG_1782.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/0925c2c11618459f9ca8bb62f3b2772c1679666004-origin-IMG_1783.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/170afcaf7f5c4e759f6ea4a7d60d58d51679666088-origin-IMG_1790.PNG\"]', '5', null, null, '4', '1', '0', '0', '111111', '2023-03-28 15:12:53', null, '2023-03-28 15:12:53');
INSERT INTO `t_img_details` VALUES ('1640613218122293250', '永远爱蜡笔小新', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/c275b4510a48489f814e021c59e36c431679667741-origin-IMG_2052.PNG', '1599618448034959361', '1594985167723864115', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/c275b4510a48489f814e021c59e36c431679667741-origin-IMG_2052.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/3de9b6b85322417eb7ccd101a19e9d021679667745-origin-IMG_2053.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/f84a87d246e24647a1164f60e9620a9a1679667749-origin-IMG_2054.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/fd9a1d94d3764342a85782eb783e98b41679667752-origin-IMG_2055.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/3e1b81dde197401eac1fc0dd82b9602d1679667778-origin-IMG_2056.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/1388d8092a60495fa24f45ec6119dc501679667785-origin-IMG_2057.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/094d5452bd4543c592d26722541ad22a1679667792-origin-IMG_2060.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/f696d8bce3644322b4ed8cc48b4e99331679673727-origin-IMG_2063.jpg\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/841c566bb8334905905e68f4d2f6e4d91679673732-origin-IMG_2064.jpg\"]', '9', null, null, '22', '3', '1', '8', '111111', '2023-03-28 15:14:07', null, '2023-04-12 19:05:23');
INSERT INTO `t_img_details` VALUES ('1640613484850667521', '柯南壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/dff6c33970a04610a47dadfa8147e8be1679666033-origin-IMG_1786.PNG', '1599618448034959361', '1594985167723864119', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/dff6c33970a04610a47dadfa8147e8be1679666033-origin-IMG_1786.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/9c22965c47eb45c8ab096e3c96024a1c1679666037-origin-IMG_1787.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b1e1206a2f634ee3accd2bbf531c5aab1679666026-origin-IMG_1785.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4f5e7bfd035b437c8a30d4ee8c81f82a1679666021-origin-IMG_1784.PNG\"]', '4', null, null, '12', '1', '0', '2', '111111', '2023-03-28 15:15:11', null, '2023-03-28 15:15:11');
INSERT INTO `t_img_details` VALUES ('1640613663507046401', '小埋小埋', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5ffeeb1535724542b8e83a970607c8641679666100-origin-IMG_1791.PNG', '1599618448034959361', '1594985167723864412', '1594985167723864066', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5ffeeb1535724542b8e83a970607c8641679666100-origin-IMG_1791.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/7673ddfc812f445eb74135f37f7fd8991679666105-origin-IMG_1792.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/c463332c492e4890886192a93d0bf3c31679666110-origin-IMG_1793.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/ca3304f8aca648fda6a936b6870e29cc1679666119-origin-IMG_1794.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/abe55d160304482d9be8fd7cda7eedca1679666132-origin-IMG_1795.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/7cbe1845e25c446c9cdd46382c6212c11679666141-origin-IMG_1796.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/db9226e198d345b28ca46b390824d8331679666151-origin-IMG_1797.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/c7a536536e83488087aabbfe0a6876681679666162-origin-IMG_1798.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/c0cfb5f018254ab5bbf4e9c0ffec7cb61679666175-origin-IMG_1799.PNG\"]', '9', null, null, '2', '0', '0', '0', '111111', '2023-03-28 15:15:53', null, '2023-03-28 15:15:53');
INSERT INTO `t_img_details` VALUES ('1640613999386910721', '猫猫', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e26add362cc94896ba2656cdd2100a181679673902-origin-IMG_2097.JPG', '1601126546037874692', '1594985167723864117', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e26add362cc94896ba2656cdd2100a181679673902-origin-IMG_2097.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b0e79cee97df479690ceb207d9c31bae1679673905-origin-IMG_2098.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/256fdadbe0ce45b79067f8fa761d8be31679673908-origin-IMG_2099.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/fce444637f2a49aa8534fc27988698ec1679673911-origin-IMG_2100.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5c18a036484f4689a6635d446a7833f41679673914-origin-IMG_2101.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/f82438286b2446caa3c17f05ebc646d61679673917-origin-IMG_2102.JPG\"]', '6', null, null, '1', '1', '0', '4', '111111', '2023-03-28 15:17:13', null, '2023-03-28 15:17:13');
INSERT INTO `t_img_details` VALUES ('1640614260939513858', '猫猫', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/ed6b6ac81f604fa9b415277a88e907fe1679643722-origin-IMG_1748.PNG', '1601126546037874692', '1594985167723864217', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/ed6b6ac81f604fa9b415277a88e907fe1679643722-origin-IMG_1748.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/2b70870e61294377b5dfebef8bf5ff121679643703-origin-IMG_1747.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/58f51b2cb5b743ab94a8d0e6ab3f988f1679643679-origin-IMG_1743.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b8f68281c25146019737f8f28a6bac901679643686-origin-IMG_1744.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/3a3a3c8da0d04e9d80a1fa3027ab19531679643701-origin-IMG_1746.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/c6cc5b6c854a468abd0ba564bf6ef4061679643695-origin-IMG_1745.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/f3a2f1ec95c840988733ea2f542c622c1679643730-origin-IMG_1749.PNG\"]', '7', null, null, '2', '0', '0', '0', '111111', '2023-03-28 15:18:16', null, '2023-03-28 15:18:16');
INSERT INTO `t_img_details` VALUES ('1640614593845616642', '风景壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/d0f7195e8c2d424fa01c11e4d2fc5ee71679643034-origin-IMG_1732.PNG', '1601126546037874692', '1594985167723864114', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/d0f7195e8c2d424fa01c11e4d2fc5ee71679643034-origin-IMG_1732.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/f8c2693f657442b3a7ca97a9d3f5c2b31679643040-origin-IMG_1733.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/7a089709654645778a9f4eb632dc0a501679643042-origin-IMG_1734.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/2a2d54e21d7e48609c9388f0697a77941679643053-origin-IMG_1735.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/698341145a6d4967a648fb094bc537eb1679643056-origin-IMG_1736.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/73348d84ec9e46c98550ca815f13454f1679643058-origin-IMG_1737.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/bf08d801291f4cb988923f22b4e6a9bc1679643655-origin-IMG_1741.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/8d620c2aeb02416593a92a6f3751b4341679643659-origin-IMG_1742.PNG\"]', '8', null, null, '27', '3', '0', '31', '111111', '2023-03-28 15:19:35', null, '2023-03-28 15:19:35');
INSERT INTO `t_img_details` VALUES ('1640614914206556161', '壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/847add066ead49bfbd619bf3555841f21679666489-origin-IMG_2025.JPG', '1601126546037874692', '1594985167723864215', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/847add066ead49bfbd619bf3555841f21679666489-origin-IMG_2025.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/a29499ab430f4712bf4b6c435854531b1679666489-origin-IMG_2026.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/aeea1e4b7b70402da486019748e64ca71679666489-origin-IMG_2027.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/608ce938c98b48fba7eae9eed1f520891679666489-origin-IMG_2028.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4b6e65e9ad2c4f6a94cd9544f81ea4f21679666489-origin-IMG_2029.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/561ff9858add425f80099c1c81e154dc1679666489-origin-IMG_2030.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/dd41e7dab7ca4890a2a02f4888f47ccf1679666489-origin-IMG_2031.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b9fa472166ce49938766f9685232504c1679666489-origin-IMG_2032.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/9d5ed9b8f177458bbff4eb33627f2f5d1679666489-origin-IMG_2033.JPG\"]', '9', null, null, '21', '1', '0', '3', '111111', '2023-03-28 15:20:51', null, '2023-03-28 15:20:51');
INSERT INTO `t_img_details` VALUES ('1640615323503517697', '风景壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5dd0936f836f4918aef4a1054c0c4e0a1679666410-origin-IMG_1924.JPG', '1601126546037874692', '1594985167723864210', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5dd0936f836f4918aef4a1054c0c4e0a1679666410-origin-IMG_1924.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/a57005c0561844ab8611f32e92220dcd1679666410-origin-IMG_1925.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5904e3a5a142452aad8e06936b47174e1679666410-origin-IMG_1926.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/2e2314157f3341e1a39fdc7ec21c64bb1679666410-origin-IMG_1927.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/0a58d8d0813a41bf9ae49ba4f60aa43b1679666410-origin-IMG_1928.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/ab58518fd4c44a4ca9d786e2c1a1d4441679666410-origin-IMG_1929.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e2f38dca6b7346d78f661337d775a1501679666410-origin-IMG_1930.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5cef530a03dc414d8c10cb206053e30d1679666410-origin-IMG_1931.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/d0c57bbe837e429b86e7c249b5642c4f1679666410-origin-IMG_1932.JPG\"]', '9', null, null, '21', '2', '1', '8', '111111', '2023-03-28 15:22:29', null, '2023-04-12 16:53:51');
INSERT INTO `t_img_details` VALUES ('1640615544300068866', '分享壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/51bed9e9bd5c48ffacfbd2f2936492851679666489-origin-IMG_2037.JPG', '1601126546037874692', '1594985167723864114', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/51bed9e9bd5c48ffacfbd2f2936492851679666489-origin-IMG_2037.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b61fc5f35fc740d1be3ed25f5e01fc591679666489-origin-IMG_2036.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5cfb62a5938a472e816a01f4943c1ef71679666489-origin-IMG_2035.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5eda2abc553947928b6a80ed4b905b531679666489-origin-IMG_2034.JPG\"]', '4', null, null, '4', '1', '0', '3', '111111', '2023-03-28 15:23:22', null, '2023-03-28 15:23:22');
INSERT INTO `t_img_details` VALUES ('1640615698050670593', '月亮壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/549c489f5f4f4e25ad2b0f8ac8421eb41679666411-origin-IMG_1954.PNG', '1601126546037874692', '1594985167723864215', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/549c489f5f4f4e25ad2b0f8ac8421eb41679666411-origin-IMG_1954.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/8bbaee2a5e38426fbf3b975403e9d4c21679666411-origin-IMG_1953.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/83b18eebf3ee442cb9911828d80970cc1679666411-origin-IMG_1952.JPG\"]', '3', null, null, '6', '2', '0', '21', '111111', '2023-03-28 15:23:58', null, '2023-03-28 15:23:58');
INSERT INTO `t_img_details` VALUES ('1640615873733287937', '风景壁纸分享', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/eb1eb7788f754b7b898d1d11dbb1afa01679666411-origin-IMG_1956.JPG', '1601126546037874692', '1594985167723864114', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/eb1eb7788f754b7b898d1d11dbb1afa01679666411-origin-IMG_1956.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/87a7ff8fdc084fa387deea9fee54f4691679666411-origin-IMG_1955.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/421ed4d495354ec6bdcf07b8cc9547751679666411-origin-IMG_1951.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/d1cdf5078e6644e38767979ab99a304c1679666411-origin-IMG_1947.JPG\"]', '4', null, null, '11', '2', '0', '28', '111111', '2023-03-28 15:24:40', null, '2023-04-12 16:38:24');
INSERT INTO `t_img_details` VALUES ('1640616099672055809', '花花\n', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/7b8145945d8c44db827868e14d8856e21679666373-origin-IMG_1820.JPG', '1601126546037874692', '1594985167723864217', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/7b8145945d8c44db827868e14d8856e21679666373-origin-IMG_1820.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/33fb4f8492f24a1db3e886a08e8e2b871679666373-origin-IMG_1821.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/bbedc73a12cf4485a80f6a6769f1a1661679666373-origin-IMG_1822.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/3809e6570e9a417f893dcbbbdea8f38d1679666373-origin-IMG_1823.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e80ec2974f244c729cc67ca346f58b011679666373-origin-IMG_1824.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/eeab0c2b3c62475bac79707a06a9c0731679666373-origin-IMG_1825.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/9085af90944a4f2d87d4b47668ffd44a1679666373-origin-IMG_1826.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/fa44988df4f044d7b09e136a860748941679666373-origin-IMG_1827.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/a883a8532bcb492a834ebf0de0b491f61679666373-origin-IMG_1828.JPG\"]', '9', null, null, '6', '1', '0', '2', '111111', '2023-03-28 15:25:34', null, '2023-03-28 15:25:34');
INSERT INTO `t_img_details` VALUES ('1640616384784064514', '花花', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/62cd478af95c4e639f9c8423c0e61ba41679666373-origin-IMG_1831.JPG', '1601126546037874692', '1594985167723864115', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/62cd478af95c4e639f9c8423c0e61ba41679666373-origin-IMG_1831.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/15d120b426a14d7c93bc6a775a56d2ff1679666373-origin-IMG_1832.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4eb2e2d3907e4ac58819071e99561f0a1679666373-origin-IMG_1833.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/9dd5d298602740be9551ca0da93c8b9c1679666373-origin-IMG_1834.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/cab1f341f2374adab7837818ea55faf81679666373-origin-IMG_1835.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/1d072abc6eb0483f819f5b2dfb93f9a91679666373-origin-IMG_1836.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/8c169546eb1849b89c87c288a518702a1679666373-origin-IMG_1837.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/082ce027611a41568ae5de042de8946e1679666373-origin-IMG_1838.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/fb4049dc29c7498780d4fbd8668e59231679666373-origin-IMG_1841.JPG\"]', '9', null, null, '4', '1', '0', '11', '111111', '2023-03-28 15:26:42', null, '2023-03-28 15:26:42');
INSERT INTO `t_img_details` VALUES ('1640616679895293953', '搞怪头像壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5651e57323ff4f6590885adf34ef0b7a1679666459-origin-IMG_1969.PNG', '1601126546037874690', '1594985167723864118', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5651e57323ff4f6590885adf34ef0b7a1679666459-origin-IMG_1969.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e7b59e4274ed4c2ca50b6ebd4208a4b91679666459-origin-IMG_1968.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/3008f2c77c354f4c86e847c16eba73a31679666459-origin-IMG_1967.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/d6bf6d9f7d244af1b997c848d4d66e281679666459-origin-IMG_1961.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/ebaf63727828455e929ed993014d4e0a1679666459-origin-IMG_1960.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4ab082da8dec49aab7563ed4e2ca57281679666459-origin-IMG_1958.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/a3d6f10b166e4df7ab619a3312258c461679666459-origin-IMG_1957.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/83f7711949184da8be446887fe3479961679666459-origin-IMG_1959.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/22e0d56a8aed45728f2b61b21c535bfb1679666459-origin-IMG_1964.PNG\"]', '9', null, null, '8', '0', '1', '2', '111111', '2023-03-28 15:27:52', null, '2023-03-28 15:27:52');
INSERT INTO `t_img_details` VALUES ('1640616974083776513', '纯色壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b58d71ed20cc4d1394fbc2bd7816187c1679666467-origin-IMG_1971.PNG', '1601126546037874690', '1594985167723864212', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b58d71ed20cc4d1394fbc2bd7816187c1679666467-origin-IMG_1971.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/41325fed62d842c2ac6f232cba836dd21679666467-origin-IMG_1972.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/0ee19e33730e4561ac0eec5738c720d71679666467-origin-IMG_1973.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/f5349f03d5b643c981fbf9f641c569361679666467-origin-IMG_1974.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5c9af9287e1943389cbb4bf486fa86ad1679666467-origin-IMG_1979.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/6c53e308c77e4bfda655e4e20ae69b3e1679666467-origin-IMG_1980.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/f50e7bd6c6884797b870a03c991a214a1679666467-origin-IMG_1981.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/80d0af4fa8dd43d286c009781b1db0ea1679666467-origin-IMG_1982.PNG\"]', '8', null, null, '14', '2', '0', '2', '111111', '2023-03-28 15:29:02', null, '2023-04-12 16:52:42');
INSERT INTO `t_img_details` VALUES ('1640617201301807106', '卡通壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/ff161314e40447c9a539b2842d8c51cc1679666481-origin-IMG_1985.PNG', '1601126546037874690', '1594985167723864118', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/ff161314e40447c9a539b2842d8c51cc1679666481-origin-IMG_1985.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/1c55a379fe744882b4ad5d7f7591b8b51679666481-origin-IMG_1986.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/305e355fc24b4d319b13a01b14d1770f1679666481-origin-IMG_1987.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/026f067d717440e59767faa299468cd51679666481-origin-IMG_1988.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/9cbb447fb2c64c62ad831fbd16f2aac31679666481-origin-IMG_1989.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/2390fb4286c24fdc884f330fb1a1f85d1679666481-origin-IMG_1991.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/22b10b2b72484637b5e8a71badd746541679666481-origin-IMG_1992.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/7b14f867c35f40ed89daf43c86f3b52e1679666481-origin-IMG_1993.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/47a750b32d3f4755a76b492d45c1bb781679666482-origin-IMG_1994.PNG\"]', '9', null, null, '12', '2', '0', '1', '111111', '2023-03-28 15:29:57', null, '2023-03-28 15:29:57');
INSERT INTO `t_img_details` VALUES ('1640617316410286081', '卡通壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/79d354e7167d4587a26a1f43564793d21679666482-origin-IMG_1998.PNG', '1601126546037874690', '1594985167723864118', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/79d354e7167d4587a26a1f43564793d21679666482-origin-IMG_1998.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/805294e098bf4eccaf4a2ee67c4d8e211679666482-origin-IMG_1999.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/dad6101f4c5b42b58f694a05a67c5df91679666482-origin-IMG_2001.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/94af6745cea04209a19d68e944d2871e1679666482-origin-IMG_2002.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/1202204c854a4ef397a35b2f152770411679666482-origin-IMG_2003.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4b0a3ff0620c410ebb2cb1141c9d23061679666482-origin-IMG_2004.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4bdb4efb3b9e4c37b76d5b469b49927c1679666482-origin-IMG_2006.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/ce1c630bd9614c2384aec8d7871f5ec91679666482-origin-IMG_2005.PNG\"]', '8', null, null, '7', '1', '0', '0', '111111', '2023-03-28 15:30:24', null, '2023-03-28 15:30:24');
INSERT INTO `t_img_details` VALUES ('1640617560791408641', '文字壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/409d1aa143154ef0a5e26e58f4a119e81679666391-origin-IMG_1891.JPG', '1601126546037874690', '1594985167723864211', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/409d1aa143154ef0a5e26e58f4a119e81679666391-origin-IMG_1891.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4a207501eb274a2d9d79f399090d91f71679666396-origin-IMG_1892.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/0773caff327c4f24a430a7a05df5dc0b1679666396-origin-IMG_1893.BMP\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/08f8866d268742058e63f17e62c5e9c01679666396-origin-IMG_1894.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4e9bb37b6c9e4002b27bddd8557f68421679666396-origin-IMG_1895.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/22ecbdbbc3ef4e8b86eb9415cfa15a681679666396-origin-IMG_1896.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/d3589d50853e4d8daa5f66057b353b801679666396-origin-IMG_1897.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e666a2ee98d640aaaf34ea2c0d4536bd1679666397-origin-IMG_1898.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/86e125f321404f9b9c468517a446d86b1679666397-origin-IMG_1899.JPG\"]', '9', null, null, '12', '2', '1', '0', '111111', '2023-03-28 15:31:22', null, '2023-04-12 16:53:40');
INSERT INTO `t_img_details` VALUES ('1640617780686184449', '聊天壁纸，文字壁纸分享', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4aa0bbd487e54e49bbfe498386af42981679666361-origin-IMG_1815.BMP', '1601126546037874690', '1594985167723864211', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4aa0bbd487e54e49bbfe498386af42981679666361-origin-IMG_1815.BMP\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/752f9ebae456429ab27c47f5dd7330671679666361-origin-IMG_1816.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/8e680bd28d174950981f71b6ec78ce0b1679666361-origin-IMG_1817.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/473838abb52f482e8403361c860434cf1679666361-origin-IMG_1818.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/645c495e81004c3faab010ba24732f461679666361-origin-IMG_1814.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e148281ddf984bacb83a4fee96049a4d1679666361-origin-IMG_1813.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/8cd1a8608d8a4ee0843f155d493c84101679666361-origin-IMG_1812.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5697ad8cf77c444a8b299763a30f48031679666361-origin-IMG_1811.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/1875c8cbb21747049fd72f30b45ee0151679666361-origin-IMG_1810.JPG\"]', '9', null, null, '10', '3', '0', '0', '111111', '2023-03-28 15:32:15', null, '2023-04-12 16:52:44');
INSERT INTO `t_img_details` VALUES ('1640619375415091202', 'ins壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/32bae7a4c54848aea0a7c7c45015f8361679666391-origin-IMG_1865.JPG', '1601126546037874690', '1594985167723864210', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/32bae7a4c54848aea0a7c7c45015f8361679666391-origin-IMG_1865.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/0f82c29f99eb42f5a0299ab90889e09a1679666391-origin-IMG_1869.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/867b64b2d2f44b119ce35df74fbe2cd31679666391-origin-IMG_1872.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b8c544e4a5b642bba81c82bd12f066b11679666391-origin-IMG_1873.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/2512b204b22f4ec590c31661ca2ce3981679666391-origin-IMG_1880.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/934a5c362c9c4f32b45dff8f7ceaa1f51679666391-origin-IMG_1885.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4e42008e96784bbdbb09c04053a5383a1679666391-origin-IMG_1884.JPG\"]', '7', null, null, '11', '2', '1', '4', '111111', '2023-03-28 15:38:35', null, '2023-03-28 15:38:35');
INSERT INTO `t_img_details` VALUES ('1640621906459451393', '蜡笔小新', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/73855ce82193488c99659738b334fd9f1679673714-origin-IMG_2062.jpg', '1601126546037874690', '1594985167723864415', '1594985167723864066', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/73855ce82193488c99659738b334fd9f1679673714-origin-IMG_2062.jpg\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/279b981af060486ab9bec86459093f391679673704-origin-IMG_2061.jpg\"]', '2', null, null, '24', '4', '1', '5', '111111', '2023-03-28 15:48:38', null, '2023-03-28 15:48:38');
INSERT INTO `t_img_details` VALUES ('1640626471053406210', '蜡笔小新壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b028519a87f14625b646ccdc7628423f1679667741-origin-IMG_2052.PNG', '1601126546037874690', '1594985167723864415', '1594985167723864066', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b028519a87f14625b646ccdc7628423f1679667741-origin-IMG_2052.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/414994187763427abbc0d0560103630a1679667745-origin-IMG_2053.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4a4117e71cb54090835c4b64cba4c9be1679667749-origin-IMG_2054.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/f99dff0c512049cc905e3cfb221b24bf1679667752-origin-IMG_2055.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/87dc69cfdd124ea3b58ca9454f00ebbc1679667778-origin-IMG_2056.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/bdd4ed61d25e4f39a7fb0ebd56da363c1679667785-origin-IMG_2057.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/6e9b5dcaf9aa4570a1ec62a7d4386ad71679667787-origin-IMG_2058.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/f1392d9985b24db8ab8b5b058984163e1679667790-origin-IMG_2059.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b6e21b09fbc14f08a132d67f25312ff81679673727-origin-IMG_2063.jpg\"]', '9', null, null, '26', '6', '2', '1', '111111', '2023-03-28 16:06:47', null, '2023-04-12 19:04:46');
INSERT INTO `t_img_details` VALUES ('1640627885532098562', '少女心', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/d0e1e28fa697495299e8704bd0c6dd6c1679666385-origin-IMG_1854.JPG', '1601126546037874693', '1594985167723864112', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/d0e1e28fa697495299e8704bd0c6dd6c1679666385-origin-IMG_1854.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/7c09f279229c4c20a25917bba99fec871679666384-origin-IMG_1853.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5f92af46f24e4a43908db6d23b6d26971679666384-origin-IMG_1852.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/aababaf680ea49e7828ea478494136b91679666384-origin-IMG_1851.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/19c0914140214298808adc0bb250e7ea1679666384-origin-IMG_1850.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/bdfa65bee68548719bbe58527ab488e21679666384-origin-IMG_1849.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/22d69cf2ae3441a6bb0c1818ecb2e0161679666384-origin-IMG_1848.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/5da31b35d6694d188e103113e48b69c21679666384-origin-IMG_1847.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/c380eea7107340f5b174ae320ebc14c91679666384-origin-IMG_1846.JPG\"]', '9', null, null, '13', '1', '0', '0', '111111', '2023-03-28 16:12:24', null, '2023-04-12 16:52:39');
INSERT INTO `t_img_details` VALUES ('1640628168987357185', 'ins风格壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/ba7c05f51a30478185cdf0f68bee728d1679664800-origin-IMG_1764.JPG', '1601126546037874693', '1594985167723864210', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/ba7c05f51a30478185cdf0f68bee728d1679664800-origin-IMG_1764.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/edba045f6f34407597a11a7fbdedef941679664800-origin-IMG_1763.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/8d290b845ca54a929a00a195999d4cb01679664800-origin-IMG_1762.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/72be9a3f4d7146908f1ed2966f65ba8c1679664800-origin-IMG_1761.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/1bcfb5c99682463fa588d4b9ec4da5721679664800-origin-IMG_1760.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/2e18949a7d334c688b9e72a13cd7742a1679664800-origin-IMG_1759.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/b94506ade192498f87da60648142db831679664800-origin-IMG_1758.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/7e25557b55aa4fbabda35fa25108343f1679664800-origin-IMG_1757.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/28d591960d084eff8b13d8411ffb14541679664800-origin-IMG_1756.JPG\"]', '9', null, null, '20', '1', '0', '7', '111111', '2023-03-28 16:13:32', null, '2023-04-12 16:38:20');
INSERT INTO `t_img_details` VALUES ('1640628406464655362', 'ins风格壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e9c2f177fc184b66b8495469125ba46f1679666391-origin-IMG_1882.JPG', '1601126546037874693', '1594985167723864210', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/e9c2f177fc184b66b8495469125ba46f1679666391-origin-IMG_1882.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/218b2c9dfb9f4f208957001766169c0f1679666391-origin-IMG_1889.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/8fb79154a976476593e489c4b77cf53f1679666391-origin-IMG_1890.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/711c778a312d4e09b80126042a06f6c61679666391-origin-IMG_1887.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/2f3980ac8b44421d99c210a1c6bf43851679666391-origin-IMG_1891.JPG\"]', '5', null, null, '13', '2', '1', '6', '111111', '2023-03-28 16:14:28', null, '2023-04-12 16:51:27');
INSERT INTO `t_img_details` VALUES ('1640628584319922177', 'ins风格壁纸推荐', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/6fc68887e0884c8e83e0b0e9a6613a021679666390-origin-IMG_1857.JPG', '1601126546037874693', '1594985167723864210', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/6fc68887e0884c8e83e0b0e9a6613a021679666390-origin-IMG_1857.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/f42316aac001476b9f672c06fce0e14d1679666390-origin-IMG_1861.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/90508fe49657435499c36fe5b7ff92461679666390-origin-IMG_1858.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/4147beda47c847a3ab3cd4efd7df66a61679666390-origin-IMG_1862.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/2c2db1073cd549ff8961a0b8d493c2001679666390-origin-IMG_1855.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/addbeb8dcaa64778ac888e6a3a0f8e381679666390-origin-IMG_1859.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/838a1cfdddae4d7e82d2fc33dfaf1bde1679666390-origin-IMG_1856.JPG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/89880e13e1b24ad78e1eb37af26864341679666390-origin-IMG_1860.JPG\"]', '8', null, null, '8', '1', '0', '0', '111111', '2023-03-28 16:15:11', null, '2023-03-28 16:15:11');
INSERT INTO `t_img_details` VALUES ('1640630462768328706', '花\n', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/25db78b2038744b7b763dd217858d244duitang_1679575878181.png', '1601126546037874691', '1594985167723864115', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/25db78b2038744b7b763dd217858d244duitang_1679575878181.png\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/806b7ce4c9b34c5492b495e2b1cf1814duitang_1679575842589.png\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/70cbd8b338764c07ac38234b0d586fb1duitang_1679575803319.png\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/fe976c264e8c459f862612a964254ba0duitang_1679575870017.png\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/28/a3b1bed53f024496bf0940e75602a702duitang_1679575859681.png\"]', '5', null, null, '6', '0', '0', '0', '111111', '2023-03-28 16:22:38', null, '2023-03-28 16:22:38');
INSERT INTO `t_img_details` VALUES ('1640959150773616642', '蜡笔小新', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/29/a2eb5aaed8fd49719b30c731f705ca954759835dd90e4b6db1ad6ea68601a54b1679643012-origin-IMG_1728.PNG', '1599618448034959361', '1594985167723864111', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/29/a2eb5aaed8fd49719b30c731f705ca954759835dd90e4b6db1ad6ea68601a54b1679643012-origin-IMG_1728.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/29/e52172cca8da4a95b8febd7742f07d3f2764537ec8f84de7b6c1ac9e15f07c171679643026-origin-IMG_1731(1).PNG\"]', '2', null, null, '35', '0', '1', '5', '111111', '2023-03-29 14:08:44', null, '2023-04-12 16:53:18');
INSERT INTO `t_img_details` VALUES ('1644990182220926977', '蜡笔小新', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/09/7dbad3e176b14456ac1059e47871a0e520220615164101_d77a8.jpeg', '1599618448034959361', '1594985167723864415', '1594985167723864066', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/09/7dbad3e176b14456ac1059e47871a0e520220615164101_d77a8.jpeg\"]', '1', null, null, '23', '1', '0', '1', '111111', '2023-04-09 17:06:37', null, '2023-04-12 19:04:44');
INSERT INTO `t_img_details` VALUES ('1645340754031710210', '蜡笔小新', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/91f952c7712a4404b99e3b71be058c4f20220615164103_46ef9.jpeg', '1601126546037874692', '1594985167723864113', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/91f952c7712a4404b99e3b71be058c4f20220615164103_46ef9.jpeg\"]', '1', null, null, '11', '0', '0', '0', '111111', '2023-04-10 16:19:39', null, '2023-04-12 16:54:01');
INSERT INTO `t_img_details` VALUES ('1645431306005831682', '风景图', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/7d59f4c64ba04a0c833a1755b752352920230129124200_db71a.jpg', '1601126546037874690', '1594985167723864114', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/7d59f4c64ba04a0c833a1755b752352920230129124200_db71a.jpg\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/259b21c81fdd4947b88315f5a4de88a620230129124200_675b5.jpg\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/3b880b429abd4e68a209bff4df65beb120230129124200_501b5.jpg\"]', '3', null, null, '7', '0', '0', '0', '111111', '2023-04-10 22:19:29', null, '2023-04-12 16:53:48');
INSERT INTO `t_img_details` VALUES ('1645432934201090050', '蜡笔小新', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/05b141e449974ba0ab0d03b5912a79be20230211231437_31e07.jpeg', '1601126546037874693', '1594985167723864111', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/05b141e449974ba0ab0d03b5912a79be20230211231437_31e07.jpeg\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/58f717f9af6045ba8a5f8cff6376701d20220615164101_d77a8.jpeg\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/4c0f04c0dd1746e48f16db11a183cd424759835dd90e4b6db1ad6ea68601a54b1679643012-origin-IMG_1728.PNG\"]', '3', null, null, '15', '3', '0', '1', '111111', '2023-04-10 22:25:57', null, '2023-04-12 18:47:33');
INSERT INTO `t_img_details` VALUES ('1645453878013517826', '壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/12a4754b433f46daa2330054fbc61b2f20230129124200_db71a.jpg', '1601126546037874693', '1594985167723864210', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/12a4754b433f46daa2330054fbc61b2f20230129124200_db71a.jpg\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/b42403491a4646baac8149cdb33fd3cb20230129124200_675b5.jpg\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/9c021efae48d419bb1779e94e332b18620230129124200_501b5.jpg\"]', '3', null, null, '15', '2', '0', '3', '111111', '2023-04-10 23:49:10', null, '2023-04-12 18:51:51');
INSERT INTO `t_img_details` VALUES ('1645635011271495681', '蜡笔小新', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/11/6d7d1a614df544519f9c8c192d0e836187dc69cfdd124ea3b58ca9454f00ebbc1679667778-origin-IMG_2056.PNG', '1601126546037874692', '1594985167723864415', '1594985167723864066', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/11/6d7d1a614df544519f9c8c192d0e836187dc69cfdd124ea3b58ca9454f00ebbc1679667778-origin-IMG_2056.PNG\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/11/b0aeb50dab7642d09459454e1a5e822120230211231437_31e07.jpeg\"]', '2', null, null, '11', '1', '0', '1', '111111', '2023-04-11 11:48:56', null, '2023-04-12 18:47:07');
INSERT INTO `t_img_details` VALUES ('1645802812481245185', '壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/11/83825d5285ab477ba48eee60fa187617IMG_20230407_181047.jpg', '1601126546037874692', '1594985167723864210', '1594985167723864062', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/11/83825d5285ab477ba48eee60fa187617IMG_20230407_181047.jpg\"]', '1', null, null, '12', '2', '1', '3', '111111', '2023-04-11 22:55:43', null, '2023-04-12 16:53:38');
INSERT INTO `t_img_details` VALUES ('1645802821087956993', '壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/11/5ba507b50f1f43e4b93f748db16ca4f2-5bcdc727a5abd43f.png', '1601126546037874690', '1594985167723864112', '1594985167723864061', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/11/5ba507b50f1f43e4b93f748db16ca4f2-5bcdc727a5abd43f.png\"]', '1', null, null, '8', '1', '0', '0', '111111', '2023-04-11 22:55:45', null, '2023-04-12 19:05:24');
INSERT INTO `t_img_details` VALUES ('1647566765456646146', '小埋的壁纸', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/16/44c296228e4447a5b3670e5515ea7ae5img-1680520993129c27f3582fb26c0546ca0441b7528f1cc8016793cd67c4c04443212bad6a9ff0d.jpg', '1601126546037874690', '1594985167723864412', '1594985167723864066', '[\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/16/44c296228e4447a5b3670e5515ea7ae5img-1680520993129c27f3582fb26c0546ca0441b7528f1cc8016793cd67c4c04443212bad6a9ff0d.jpg\",\"https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/16/41376d00b14a4b9c8883752ca32f4232img-1680520999887e8aad31c70a6a3e1ca6d3add0315d7b655874ebbc870598636c17ac7c98bc35b.jpg\"]', '2', null, null, '3', '1', '0', '1', '111111', '2023-04-16 19:45:02', null, '2023-04-16 19:45:02');

-- ----------------------------
-- Table structure for t_message
-- ----------------------------
DROP TABLE IF EXISTS `t_message`;
CREATE TABLE `t_message` (
  `id` bigint NOT NULL,
  `send_id` bigint DEFAULT NULL,
  `accept_id` bigint DEFAULT NULL,
  `content` longtext,
  `time` longtext,
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_message
-- ----------------------------

-- ----------------------------
-- Table structure for t_message_user_relation
-- ----------------------------
DROP TABLE IF EXISTS `t_message_user_relation`;
CREATE TABLE `t_message_user_relation` (
  `id` bigint NOT NULL,
  `send_id` bigint DEFAULT NULL,
  `accept_id` bigint DEFAULT NULL,
  `count` int DEFAULT '0',
  `content` longtext,
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_message_user_relation
-- ----------------------------

-- ----------------------------
-- Table structure for t_tag
-- ----------------------------
DROP TABLE IF EXISTS `t_tag`;
CREATE TABLE `t_tag` (
  `id` bigint NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `count` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_tag
-- ----------------------------
INSERT INTO `t_tag` VALUES ('1640610678118899713', '蜡笔小新', null, null, null, '12');
INSERT INTO `t_tag` VALUES ('1640610678131482626', '壁纸', null, null, null, '45');
INSERT INTO `t_tag` VALUES ('1640610678131482627', '头像', null, null, null, '13');
INSERT INTO `t_tag` VALUES ('1640610989915070465', '猫咪', null, null, null, '4');
INSERT INTO `t_tag` VALUES ('1640610989927653378', '可爱', null, null, null, '6');
INSERT INTO `t_tag` VALUES ('1640611886263001089', '麻衣', null, null, null, '1');
INSERT INTO `t_tag` VALUES ('1640612106157776898', '火影忍者', null, null, null, '1');
INSERT INTO `t_tag` VALUES ('1640612106157776899', '鸣人', null, null, null, '1');
INSERT INTO `t_tag` VALUES ('1640612431719653378', '夏目', null, null, null, '1');
INSERT INTO `t_tag` VALUES ('1640612431732236289', '动漫', null, null, null, '4');
INSERT INTO `t_tag` VALUES ('1640612907588608002', '名侦探柯南', null, null, null, '2');
INSERT INTO `t_tag` VALUES ('1640613484867444738', '情侣壁纸', null, null, null, '1');
INSERT INTO `t_tag` VALUES ('1640613663523823617', '小埋', null, null, null, '2');
INSERT INTO `t_tag` VALUES ('1640614593858199553', '风景', null, null, null, '8');
INSERT INTO `t_tag` VALUES ('1640614593858199554', '旅游', null, null, null, '2');
INSERT INTO `t_tag` VALUES ('1640615698080030721', '月亮', null, null, null, '1');
INSERT INTO `t_tag` VALUES ('1640616099688833026', '花花', null, null, null, '3');
INSERT INTO `t_tag` VALUES ('1640616679928848385', '搞怪', null, null, null, '1');
INSERT INTO `t_tag` VALUES ('1640616974096359425', '纯色壁纸', null, null, null, '1');
INSERT INTO `t_tag` VALUES ('1640617201314390017', '卡通', null, null, null, '2');
INSERT INTO `t_tag` VALUES ('1640617560812380161', '文字壁纸', null, null, null, '2');
INSERT INTO `t_tag` VALUES ('1640619375431868419', 'ins', null, null, null, '5');

-- ----------------------------
-- Table structure for t_tag_img_relation
-- ----------------------------
DROP TABLE IF EXISTS `t_tag_img_relation`;
CREATE TABLE `t_tag_img_relation` (
  `id` bigint NOT NULL,
  `mid` bigint DEFAULT NULL,
  `tag_ids` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_tag_img_relation
-- ----------------------------
INSERT INTO `t_tag_img_relation` VALUES ('1640610678144065537', '1640610678102122497', '1640610678118899713;1640610678131482626;1640610678131482627;');
INSERT INTO `t_tag_img_relation` VALUES ('1640610989927653379', '1640610989889904642', '1640610678131482626;1640610989915070465;1640610989927653378;');
INSERT INTO `t_tag_img_relation` VALUES ('1640611226637393922', '1640611226570285058', '1640610678131482626;1640610678131482627;1640610989915070465;');
INSERT INTO `t_tag_img_relation` VALUES ('1640611505327923201', '1640611505290174465', '1640610678118899713;1640610678131482627;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640611666837987330', '1640611666812821506', '1640610678118899713;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640611886275584001', '1640611886250418177', '1640611886263001089;1640610678131482626;1640610678131482627;');
INSERT INTO `t_tag_img_relation` VALUES ('1640612106174554113', '1640612106140999681', '1640612106157776898;1640612106157776899;');
INSERT INTO `t_tag_img_relation` VALUES ('1640612431732236290', '1640612431702876162', '1640612431719653378;1640610678131482626;1640612431732236289;');
INSERT INTO `t_tag_img_relation` VALUES ('1640612631452409857', '1640612631418855426', '1640612431732236289;1640610989927653378;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640612907601190914', '1640612907571830785', '1640612907588608002;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640613218164236289', '1640613218122293250', '1640610678118899713;1640610678131482626;1640612431732236289;');
INSERT INTO `t_tag_img_relation` VALUES ('1640613484892610562', '1640613484850667521', '1640613484867444738;1640610678131482626;1640612907588608002;');
INSERT INTO `t_tag_img_relation` VALUES ('1640613663536406530', '1640613663507046401', '1640613663523823617;1640610678131482626;1640610678131482627;');
INSERT INTO `t_tag_img_relation` VALUES ('1640613999416270849', '1640613999386910721', '1640610989915070465;1640610678131482626;1640610678131482627;');
INSERT INTO `t_tag_img_relation` VALUES ('1640614260964679681', '1640614260939513858', '1640610989915070465;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640614593870782465', '1640614593845616642', '1640614593858199553;1640614593858199554;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640614914235916289', '1640614914206556161', '1640610678131482626;1640614593858199553;');
INSERT INTO `t_tag_img_relation` VALUES ('1640615323532877825', '1640615323503517697', '1640610678131482626;1640614593858199553;');
INSERT INTO `t_tag_img_relation` VALUES ('1640615544337817602', '1640615544300068866', '1640610678131482626;1640614593858199553;');
INSERT INTO `t_tag_img_relation` VALUES ('1640615698080030722', '1640615698050670593', '1640610678131482626;1640614593858199553;1640615698080030721;');
INSERT INTO `t_tag_img_relation` VALUES ('1640615873750065154', '1640615873733287937', '1640614593858199553;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640616099701415937', '1640616099672055809', '1640616099688833026;1640610678131482627;');
INSERT INTO `t_tag_img_relation` VALUES ('1640616384813424642', '1640616384784064514', '1640616099688833026;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640616679928848386', '1640616679895293953', '1640610678131482627;1640616679928848385;1640610989927653378;');
INSERT INTO `t_tag_img_relation` VALUES ('1640616974108942337', '1640616974083776513', '1640616974096359425;1640610678131482626;1640610678131482627;');
INSERT INTO `t_tag_img_relation` VALUES ('1640617201314390018', '1640617201301807106', '1640617201314390017;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640617316448034818', '1640617316410286081', '1640617201314390017;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640617560829157377', '1640617560791408641', '1640610678131482626;1640617560812380161;1640610678131482627;');
INSERT INTO `t_tag_img_relation` VALUES ('1640617780715544577', '1640617780686184449', '1640617560812380161;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640619375457034242', '1640619375415091202', '1640619375431868419;1640610678131482626;1640610678131482627;');
INSERT INTO `t_tag_img_relation` VALUES ('1640621906488811522', '1640621906459451393', '1640610678118899713;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640626471204401154', '1640626471053406210', '1640610678118899713;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640627885574041602', '1640627885532098562', '1640610678131482626;1640610989927653378;1640619375431868419;');
INSERT INTO `t_tag_img_relation` VALUES ('1640628169025105921', '1640628168987357185', '1640619375431868419;1640610678131482626;1640610678131482627;');
INSERT INTO `t_tag_img_relation` VALUES ('1640628406514987010', '1640628406464655362', '1640619375431868419;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640628584357670914', '1640628584319922177', '1640619375431868419;1640610989927653378;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640630462810271746', '1640630462768328706', '1640616099688833026;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1640959150916222978', '1640959150773616642', '1640610678118899713;1640610678131482626;1640612431732236289;');
INSERT INTO `t_tag_img_relation` VALUES ('1644990182325784577', '1644990182220926977', '1640610678118899713;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1645340754082041858', '1645340754031710210', '1640610678118899713;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1645431306102300673', '1645431306005831682', '1640614593858199553;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1645432934255616001', '1645432934201090050', '1640610678118899713;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1645453878089015297', '1645453878013517826', '1640614593858199553;1640610678131482626;1640614593858199554;');
INSERT INTO `t_tag_img_relation` VALUES ('1645635012051636226', '1645635011271495681', '1640610678118899713;1640610678131482627;1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1645802812531576833', '1645802812481245185', '1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1645802821171843073', '1645802821087956993', '1640610678131482626;');
INSERT INTO `t_tag_img_relation` VALUES ('1647566765544726529', '1647566765456646146', '1640610678131482626;1640610989927653378;1640613663523823617;');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` bigint NOT NULL,
  `user_id` bigint DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `avatar` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `gender` tinyint DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `description` longtext,
  `status` tinyint DEFAULT NULL,
  `cover` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `birthday` varchar(50) DEFAULT NULL,
  `address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `updater` bigint DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1599618448034959361', '8034959361', 'xiaozhao', 'e10adc3949ba59abbe56e057f20f883e', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/29/f2be12b42f76495e9e21e2e0b2819f6dduitang_1679575859681.png', '1', '18572755162', '484235492@qq.com', '这是小赵的空间\n\n', null, 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/03/29/140c37d70e544453843f23b59e170ab8duitang_1678928145596.png', null, null, null, '2023-03-24 14:47:58', null, '2023-04-12 21:14:21');
INSERT INTO `t_user` VALUES ('1601126546037874690', '6037874690', 'qwer', 'e10adc3949ba59abbe56e057f20f883e', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/10/b98f847362c447cf9c8a0d911dc6fe4e20220615164101_d77a8.jpeg', '0', '13076584563', '3044606584@qq.com', null, null, 'https://img1.baidu.com/it/u=1681825099,2898845545&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=888', null, null, null, '2023-03-24 14:48:56', null, '2023-04-10 17:31:35');
INSERT INTO `t_user` VALUES ('1601126546037874691', '6037874691', 'xzjsccz', 'e10adc3949ba59abbe56e057f20f883e', 'https://img2.baidu.com/it/u=4061455589,2774244664&fm=253&fmt=auto&app=138&f=JPEG?w=501&h=500', '1', null, null, null, null, 'https://img0.baidu.com/it/u=4114912361,4128907800&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=889', null, null, null, '2023-03-24 15:27:52', null, '2023-03-24 15:27:59');
INSERT INTO `t_user` VALUES ('1601126546037874692', '6037874692', 'ccz', 'e10adc3949ba59abbe56e057f20f883e', 'https://img2.baidu.com/it/u=1196632544,116923444&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', '0', null, null, '这个人太懒了', null, 'https://img1.baidu.com/it/u=2785988489,2898142966&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', null, null, null, '2023-03-24 15:27:54', null, '2023-04-10 17:05:18');
INSERT INTO `t_user` VALUES ('1601126546037874693', '6037874693', 'qwer1234', 'e10adc3949ba59abbe56e057f20f883e', 'https://img1.baidu.com/it/u=572139434,3677211895&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', '1', null, null, null, null, 'https://img0.baidu.com/it/u=3574495855,3620224580&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=888', null, null, null, '2023-03-24 15:27:57', null, '2023-03-24 15:28:04');
INSERT INTO `t_user` VALUES ('1646790471022325762', '3391338551', 'tlie9mhn986', 'd41d8cd98f00b204e9800998ecf8427e', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/14/04a039b7bd8a4610bcb000279875fea1IMG_20230205_204418.jpg', null, '17794297797', null, null, null, 'https://cube.elemecdn.com/3/7c/3ea6beec64369c2642b92c6726f1epng.png', null, null, '111111', '2023-04-14 16:20:19', null, '2023-04-14 22:08:07');
INSERT INTO `t_user` VALUES ('1646901667952111618', '7304910623', '小张', 'd41d8cd98f00b204e9800998ecf8427e', 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/15/deff81ded0f3461daa1b9d0e8f87741fmmexport1681487948316.jpg', null, '18298478014', null, null, null, 'https://cc-video-oss.oss-accelerate.aliyuncs.com/2023/04/15/c7df3d72b6ee4b8f957e2f537786603cmmexport1681487954754.jpg', null, null, '111111', '2023-04-14 23:42:10', null, '2023-04-15 00:00:49');

-- ----------------------------
-- Table structure for t_user_other_login_relation
-- ----------------------------
DROP TABLE IF EXISTS `t_user_other_login_relation`;
CREATE TABLE `t_user_other_login_relation` (
  `id` bigint NOT NULL,
  `uid` bigint DEFAULT NULL,
  `other_user_id` varchar(50) DEFAULT NULL,
  `other_username` varchar(100) DEFAULT NULL,
  `other_avatar` varchar(255) DEFAULT NULL,
  `other_token` varchar(255) DEFAULT NULL,
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_user_other_login_relation
-- ----------------------------

-- ----------------------------
-- Table structure for t_user_record
-- ----------------------------
DROP TABLE IF EXISTS `t_user_record`;
CREATE TABLE `t_user_record` (
  `id` bigint NOT NULL,
  `uid` bigint DEFAULT NULL,
  `trend_count` bigint DEFAULT '0',
  `follow_count` bigint DEFAULT '0',
  `fan_count` bigint DEFAULT '0',
  `noreply_count` bigint DEFAULT '0',
  `agree_collection_count` bigint DEFAULT '0',
  `nochat_count` bigint DEFAULT '0',
  `creator` bigint DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_user_record
-- ----------------------------
INSERT INTO `t_user_record` VALUES ('1599618448034959370', '1599618448034959361', '15', '0', '0', '0', '0', '0', null, '2023-03-26 12:17:01');
INSERT INTO `t_user_record` VALUES ('1599618448034959371', '1601126546037874690', '12', '0', '0', '1', '0', '-15', null, '2023-03-26 12:17:03');
INSERT INTO `t_user_record` VALUES ('1599618448034959372', '1601126546037874691', '1', '0', '0', '0', '0', '0', null, '2023-03-26 12:17:06');
INSERT INTO `t_user_record` VALUES ('1599618448034959373', '1601126546037874692', '13', '0', '0', '6', '1', '-9', null, '2023-03-26 12:17:08');
INSERT INTO `t_user_record` VALUES ('1599618448034959374', '1601126546037874693', '6', '0', '0', '0', '0', '0', null, '2023-03-26 12:17:10');
INSERT INTO `t_user_record` VALUES ('1646790471093628929', '1646790471022325762', '0', '0', '0', '0', '0', '0', '111111', '2023-04-14 16:20:19');
INSERT INTO `t_user_record` VALUES ('1646901667994054658', '1646901667952111618', '0', '0', '0', '0', '0', '0', '111111', '2023-04-14 23:42:10');
